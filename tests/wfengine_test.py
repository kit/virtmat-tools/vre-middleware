"""Unit tests for the wfengine module"""
import uuid
import unittest
import threading
import os
import time
from tempfile import NamedTemporaryFile, TemporaryDirectory, mkdtemp
import pytest
from fireworks.core.launchpad import LaunchPad
from fireworks.fw_config import LAUNCHPAD_LOC, QUEUEADAPTER_LOC
from fireworks import Firework, Workflow, PyTask, ScriptTask
from fireworks.user_objects.queue_adapters.common_adapter import CommonAdapter
from testfixtures import compare
from virtmat.middleware.resconfig import WorkerConfig, ResConfig
from virtmat.middleware.engine.wfengine import WFEngine
from virtmat.middleware.exceptions import ConfigurationException, SlurmError
from virtmat.middleware.exceptions import InvalidStateException, TestingException
from virtmat.middleware.utilities import get_slurm_job_state, await_slurm_job_state
from virtmat.middleware.utilities import exec_cancel

# the test script creates a QUEUEADAPTER_LOC file for the CI pipeline
# this qadapter dict can be used only locally
QADAPTER_DCT = {
    '_fw_name': 'CommonAdapter',
    '_fw_q_type': 'SLURM',
    'nodes': 1,
    'ntasks': 1,
    'pre_rocket': '. python-3.6.8/bin/activate',
    'queue': 'dev_single',
    'rocket_launch': 'rlaunch singleshot',
    'walltime': '00:01:00'
}


class ResConfigSetup(unittest.TestCase):
    """base class for all classes testing WFEngine"""
    def setUp(self):
        wcfg1 = WorkerConfig(name='workflow1')
        wcfg2 = WorkerConfig(name='workflow2')
        rcfg = ResConfig(workers=[wcfg1, wcfg2], default_worker=wcfg1)
        with NamedTemporaryFile(mode='w', prefix='resconfig-', suffix='.yaml',
                                encoding='utf-8', delete=False) as fh:
            self.resconfig_loc = fh.name
        rcfg.to_file(self.resconfig_loc)
        os.environ['RESCONFIG_LOC'] = self.resconfig_loc

    def tearDown(self):
        os.unlink(self.resconfig_loc)
        del os.environ['RESCONFIG_LOC']


class WFEngineTest(ResConfigSetup):
    """test the WFEngine class without SLURM"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

        self.launchdir = os.getcwd()
        self.unique_launchdir = False
        self.sleep_time = 30
        self.name = str(uuid.uuid4())
        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        tasks = [PyTask(func='math.pow', inputs=['source', 'power'],
                        outputs=['number'])]
        spec = {'source': 2, 'power': 2, '_category': 'interactive'}
        fw_1 = Firework(tasks=tasks, spec=spec, name='Power task')
        tasks = [PyTask(func='math.sqrt', inputs=['number'])]
        spec = {'_category': 'interactive'}
        fw_2 = Firework(tasks=tasks, spec=spec, name='Square root task')
        fws = [fw_1, fw_2]
        links = {fws[0]: [fws[1]], fws[1]: []}
        workflow = Workflow(fws, links_dict=links, name=wf_name, metadata={})
        self.launchpad.add_wf(workflow)
        self.wf_ids = self.launchpad.get_wf_ids(self.wf_query)
        nodes_query = {'nodes': {'$in': self.wf_ids}}
        self.fw_ids = self.launchpad.get_fw_ids_in_wfs(nodes_query)
        self.wfe = WFEngine(launchpad=self.launchpad, wf_query=self.wf_query,
                            name='workflow1', unique_launchdir=self.unique_launchdir)

    def tearDown(self):
        if self.wfe.thread is not None and self.wfe.thread.is_alive():
            self.wfe.stop()
            self.wfe.thread.join()
        self.launchpad.delete_wf(self.wf_ids[0])
        super().tearDown()

    def test_launchdir(self):
        """test the launchdir"""
        self.assertEqual(self.wfe.launchdir, self.launchdir)

    def test_start(self):
        """test staring the launcher thread"""
        self.wfe.start()
        self.assertTrue(self.wfe.thread.is_alive())
        self.wfe.stop()

    def test_stop(self):
        """test stopping the launcher thread"""
        self.wfe.start()
        self.wfe.stop()
        self.wfe.thread.join()
        self.assertFalse(self.wfe.thread.is_alive())

    def test_get_fw_ids(self):
        """test the number of nodes in workflow"""
        self.assertEqual(len(self.wfe.fw_ids), 2)

    def test_wf_query_setter_getter(self):
        """test setting wf_query and getting fw_ids and wf_ids properties"""
        self.assertEqual(len(self.wfe.fw_ids), 2)
        new_q = {'name': str(uuid.uuid4())}
        self.wfe.wf_query = new_q
        self.assertEqual(self.wfe.wf_query, new_q)
        self.assertEqual(len(self.wfe.fw_ids), 0)
        self.assertEqual(len(self.wfe.wf_ids), 0)
        self.wfe.wf_query = self.wf_query
        self.assertEqual(self.wfe.wf_query, self.wf_query)
        self.assertEqual(len(self.wfe.fw_ids), 2)
        self.assertEqual(len(self.wfe.wf_ids), 1)

    def test_wf_ids_setter_getter(self):
        """test setting wf_ids and getting wf_query and fw_ids properties"""
        self.assertEqual(len(self.wfe.wf_ids), 1)
        self.wfe.wf_ids = []
        self.assertEqual(self.wfe.wf_query, {'nodes': {'$in': []}})
        self.assertEqual(len(self.wfe.fw_ids), 0)
        self.assertEqual(len(self.wfe.wf_ids), 0)
        self.wfe.wf_ids = self.wf_ids
        self.assertEqual(self.wfe.wf_ids, self.wf_ids)
        self.assertEqual(len(self.wfe.fw_ids), 2)
        self.assertEqual(len(self.wfe.wf_ids), 1)

    def test_name_setter_getter(self):
        """test the setter / getter name property"""
        self.assertEqual(self.wfe.name, 'workflow1')
        self.wfe.name = 'workflow2'
        self.assertEqual(self.wfe.name, 'workflow2')
        with self.assertRaises(ConfigurationException) as context:
            self.wfe.name = 'workflow3'
        errmsg = 'worker workflow3 not found in list of workers'
        self.assertIn(errmsg, str(context.exception))

    def test_status_summary(self):
        """test printing status summary"""
        print(self.wfe.status_summary())

    def test_status_detail(self):
        """test printing status details"""
        print(self.wfe.status_detail(self.fw_ids[0]))

    def test_launcher(self):
        """test launcher function"""
        event = threading.Event()
        event.set()
        self.wfe.launcher(event)

    def test_add_node(self):
        """test add_node function"""
        fw_ids = self.launchpad.get_fw_ids_in_wfs(self.wf_query)
        self.assertEqual(len(fw_ids), 2)
        fw_query = {'name': 'Power task'}
        fw_ids = self.launchpad.get_fw_ids_in_wfs(self.wf_query, fw_query)
        self.assertEqual(len(fw_ids), 1)
        inputs = [(fw_ids[0], 'number', None)]
        self.wfe.add_node(func='math.log2', inputs=inputs, outputs=['log'])
        fw_ids = self.launchpad.get_fw_ids_in_wfs(self.wf_query)
        self.assertEqual(len(fw_ids), 3)

    def test_add_node_no_parents(self):
        """test the add_node function with no valid parent nodes"""
        with self.assertRaises(ConfigurationException) as context:
            self.wfe.add_node(func='math.log2', inputs=[(None, 'number', 4)])
        self.assertIn('no valid parent nodes defined', str(context.exception))

    def test_update_node(self):
        """test update_node function"""
        new_name = str(uuid.uuid4())
        self.wfe.update_node(self.wf_ids[0], {'name': new_name})
        updated_name = self.launchpad.get_fw_by_id(self.wf_ids[0]).spec['name']
        self.assertEqual(updated_name, new_name)

    def test_add_workflow(self):
        """test adding a new workflow"""
        with self.assertRaises(ConfigurationException):
            self.wfe.add_workflow()
        tasks = [PyTask(func='math.pow', inputs=['source', 'power'],
                        outputs=['number'])]
        spec = {'source': 2, 'power': 2}
        fw_1 = Firework(tasks=tasks, spec=spec, name='Power task')
        tasks = [PyTask(func='math.sqrt', inputs=['number'])]
        spec = {}
        fw_2 = Firework(tasks=tasks, spec=spec, name='Square root task')
        fws = [fw_1, fw_2]
        links = {fws[0]: [fws[1]], fws[1]: []}
        workflow = Workflow(fws, links_dict=links, name=str(uuid.uuid4()))
        self.assertEqual(len(self.wfe.wf_ids), 1)
        self.assertEqual(len(self.wfe.fw_ids), 2)
        self.wfe.add_workflow(workflow=workflow)
        self.assertEqual(len(self.wfe.wf_ids), 2)
        self.assertEqual(len(self.wfe.fw_ids), 4)
        wf_id_added = self.launchpad.get_wf_ids({'name': workflow.name})
        self.launchpad.delete_wf(wf_id_added[0])
        # add a workflow that is already in the engine
        with self.assertRaises(ConfigurationException):
            self.wfe.add_workflow(fw_id=self.fw_ids[0])

    def test_remove_workflow(self):
        """test removing a workflow from the engine"""
        self.assertEqual(len(self.wfe.wf_ids), 1)
        self.assertEqual(len(self.wfe.fw_ids), 2)
        self.wfe.remove_workflow(self.wf_ids[0])
        self.assertEqual(len(self.wfe.wf_ids), 0)
        self.assertEqual(len(self.wfe.fw_ids), 0)

    def test_to_dict(self):
        """test saving to dictionary"""
        dict_to_compare = {
            'launchpad': self.launchpad,
            'qadapter': None,
            'wf_query': {'nodes': {'$in': self.wf_ids}},
            'name': self.name,
            'launchdir': self.launchdir,
            'unique_launchdir': self.unique_launchdir,
            'sleep_time': self.sleep_time
        }
        dict_dumped = self.wfe.to_dict()
        self.assertCountEqual(dict_to_compare, dict_dumped)

    def test_from_dict(self):
        """test loading from dictionary"""
        dump_dict = self.wfe.to_dict()
        # create new object using the values dumped in the dict
        obj = WFEngine.from_dict(dump_dict)
        self.assertEqual(type(self.wfe.launchpad), type(obj.launchpad))
        self.assertEqual(type(self.wfe.qadapter), type(obj.qadapter))
        self.assertEqual((self.wfe.launchdir), (obj.launchdir))
        self.assertEqual((self.wfe.sleep_time), (obj.sleep_time))
        self.assertEqual(type(self.wfe), type(obj))
        compare(self.wfe, obj)


class WFEngineTestLaunchdir(ResConfigSetup):
    """test the WFEngine class with launchdir"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()
        self.launchdir = mkdtemp()
        self.wfe = WFEngine(launchpad=self.launchpad, launchdir=self.launchdir)

    def tearDown(self):
        os.rmdir(self.launchdir)
        super().tearDown()

    def test_name_launchdir(self):
        """test default worker name and override default launchdir"""
        self.assertEqual(self.wfe.launchdir, self.launchdir)
        self.assertEqual(self.wfe.name, 'workflow1')


class ResConfigSetupLaunchdir(ResConfigSetup):
    """base class for testing WFEngine with default launchdir"""
    def setUp(self):
        super().setUp()
        rcfg = ResConfig.from_file(self.resconfig_loc)
        self.launchdir = mkdtemp()
        rcfg.default_worker.default_launchdir = self.launchdir
        rcfg.to_file(self.resconfig_loc)

    def tearDown(self):
        os.rmdir(self.launchdir)
        super().tearDown()


class WFEngineTestDefaultLaunchdir(ResConfigSetupLaunchdir):
    """test the WFEngine class with default launchdir in resconfig"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()
        self.wfe = WFEngine(launchpad=self.launchpad)

    def test_launchdir(self):
        """test default launchdir from resconfig"""
        self.assertEqual(self.wfe.launchdir, self.launchdir)


class WFEngineTestNonExistingLaunchdir(ResConfigSetupLaunchdir):
    """test the WFEngine class with non-existing launchdir"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

    def test_non_existing_launchdir(self):
        """test default non-exising launchdir"""
        with self.assertRaises(ConfigurationException):
            WFEngine(launchpad=self.launchpad, launchdir='/blah')


@pytest.mark.slurm
class WFEngineTestSLURM(ResConfigSetup):
    """test the WFEngine class with SLURM"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

        if QUEUEADAPTER_LOC:
            self.qadapter = CommonAdapter.from_file(QUEUEADAPTER_LOC)
        else:
            self.qadapter = CommonAdapter.from_dict(QADAPTER_DCT)

        self.launchdir = os.getcwd()
        self.sleep_time = 30
        self.name = str(uuid.uuid4())
        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        tasks = [PyTask(func='math.pow', inputs=['source', 'power'],
                        outputs=['number'])]
        spec = {'source': 2, 'power': 2, '_category': 'batch'}
        fw_1 = Firework(tasks=tasks, spec=spec, name='Power task')
        tasks = [PyTask(func='math.sqrt', inputs=['number'])]
        spec = {'_category': 'batch'}
        fw_2 = Firework(tasks=tasks, spec=spec, name='Square root task')
        fws = [fw_1, fw_2]
        links = {fws[0]: [fws[1]], fws[1]: []}
        workflow = Workflow(fws, links_dict=links, name=wf_name, metadata={})
        self.launchpad.add_wf(workflow)
        self.wf_ids = self.launchpad.get_wf_ids(self.wf_query)
        nodes_query = {'nodes': {'$in': self.wf_ids}}
        self.fw_ids = self.launchpad.get_fw_ids_in_wfs(nodes_query)
        self.wfe = WFEngine(launchpad=self.launchpad, qadapter=self.qadapter,
                            wf_query=self.wf_query)

    def tearDown(self):
        if self.wfe.thread is not None and self.wfe.thread.is_alive():
            self.wfe.stop()
            self.wfe.thread.join()
        self.launchpad.delete_wf(self.wf_ids[0])
        super().tearDown()

    def test_qlaunch(self):
        """test submitting queue jobs"""
        for fw_id in self.fw_ids:
            state = self.launchpad.get_fw_by_id(fw_id).state
            self.wfe.qlaunch(fw_id)
            res_id = self.launchpad.get_reservation_id_from_fw_id(fw_id)
            if state == 'READY':
                self.assertTrue(isinstance(res_id, str))
            else:
                self.assertEqual(state, 'WAITING')
                self.assertTrue(res_id is None)

    def test_get_lost_jobs_empty(self):
        """test lost jobs in the case of no lost jobs"""
        self.assertEqual(len(self.wfe.get_lost_jobs()), 0)

    def test_get_unreserved_nodes_empty(self):
        """test unreserved nodes in the case of no unreserved nodes"""
        self.assertEqual(len(self.wfe.get_unreserved_nodes()), 0)


class WFEngineTestRlaunch(ResConfigSetup):
    """test the rlaunch() method of WFEngine class"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        spec = {'_category': 'interactive'}
        task = ScriptTask.from_str('')
        workflow = Workflow([Firework(task, spec=spec)], name=wf_name)
        self.launchpad.add_wf(workflow)
        self.wf_ids = self.launchpad.get_wf_ids(self.wf_query)
        nodes_query = {'nodes': {'$in': self.wf_ids}}
        self.fw_ids = self.launchpad.get_fw_ids_in_wfs(nodes_query)
        self.wfe = WFEngine(launchpad=self.launchpad, qadapter=None,
                            wf_query=self.wf_query)

    def tearDown(self):
        self.launchpad.delete_wf(self.wf_ids)
        super().tearDown()

    def test_rlaunch(self):
        """test the rlaunch function"""
        init_dir = os.getcwd()
        with TemporaryDirectory() as launch_dir:
            try:
                self.wfe.launchdir = launch_dir
                self.wfe.rlaunch(self.fw_ids[0])
                contents = os.listdir(launch_dir)
                self.assertTrue(all(x.startswith('launcher_') for x in contents))
                self.assertEqual(len(contents), 1)
                self.assertEqual(os.getcwd(), init_dir)
            finally:
                os.chdir(init_dir)

    def test_rlaunch_unique_launchdir_disabled(self):
        """test the rlaunch function with disabled unique launchdir"""
        self.wfe.unique_launchdir = False
        init_dir = os.getcwd()
        with TemporaryDirectory() as launch_dir:
            try:
                self.wfe.launchdir = launch_dir
                self.wfe.rlaunch(self.fw_ids[0])
                contents = os.listdir(launch_dir)
                self.assertFalse(any(x.startswith('launcher_') for x in contents))
                self.assertTrue('FW.json' in contents)
                self.assertEqual(len(contents), 1)
                self.assertEqual(os.getcwd(), init_dir)
            finally:
                os.chdir(init_dir)


@pytest.mark.slurm
class WFEngineTestCancelJob(ResConfigSetup):
    """test the cancel_job() method of WFEngine class"""

    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

        if QUEUEADAPTER_LOC:
            qadapter = CommonAdapter.from_file(QUEUEADAPTER_LOC)
        else:
            qadapter = CommonAdapter.from_dict(QADAPTER_DCT)

        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        tasks = [PyTask(func='time.sleep', args=[40])]
        spec = {'_category': 'batch'}
        workflow = Workflow([Firework(tasks=tasks, spec=spec)], name=wf_name)
        self.launchpad.add_wf(workflow)
        self.wf_ids = self.launchpad.get_wf_ids(self.wf_query)
        nodes_query = {'nodes': {'$in': self.wf_ids}}
        self.fw_id = self.launchpad.get_fw_ids_in_wfs(nodes_query)[0]
        self.wfe = WFEngine(launchpad=self.launchpad, qadapter=qadapter,
                            wf_query=self.wf_query)

    def tearDown(self):
        self.launchpad.delete_wf(self.wf_ids)
        super().tearDown()

    def test_cancel_other_states(self):
        """test to cancel a job in other states"""
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'READY':
            raise TestingException('node is not in READY state')
        with self.assertRaises(InvalidStateException):
            self.wfe.cancel_job(self.fw_id, deactivate=True)

    def test_cancel_running_job(self):
        """test to cancel a running job"""
        self.wfe.qlaunch(self.fw_id)
        res_id = self.launchpad.get_reservation_id_from_fw_id(self.fw_id)
        time.sleep(10)
        try:
            await_slurm_job_state(res_id, 'RUNNING')
        except SlurmError:
            pytest.skip('could not reach needed SLURM job state')
        time.sleep(20)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'RUNNING':
            pytest.skip('node is not in RUNNING state')
        self.wfe.cancel_job(self.fw_id, deactivate=True)
        new_state = self.launchpad.get_fw_by_id(self.fw_id).state
        self.assertEqual(new_state, 'DEFUSED')

    def test_cancel_reserved_job(self):
        """test to cancel a reserved job"""
        self.wfe.qlaunch(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'RESERVED':
            raise TestingException('node is not in RESERVED state')
        try:
            self.wfe.cancel_job(self.fw_id, restart=True)
        except SlurmError:
            pytest.skip('could not cancel SLURM job or check SLURM job state')
        new_state = self.launchpad.get_fw_by_id(self.fw_id).state
        self.assertEqual(new_state, 'READY')


class WFEngineTestChangeNodeState(ResConfigSetup):
    """test update_node, rerun_node, update_rerun_node, cancel_job methods"""

    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        tasks = [PyTask(func='time.sleep', args=[0])]
        spec = {'_category': 'interactive'}
        workflow = Workflow([Firework(tasks=tasks, spec=spec)], name=wf_name)
        self.launchpad.add_wf(workflow)
        self.wf_ids = self.launchpad.get_wf_ids(self.wf_query)
        self.wfe = WFEngine(launchpad=self.launchpad, wf_query=self.wf_query)
        self.fw_id = self.wfe.fw_ids[0]
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'READY':
            raise TestingException('node is not in READY state')

    def tearDown(self):
        self.launchpad.delete_wf(self.wf_ids)
        super().tearDown()

    def test_not_configured_in_engine(self):
        """test changing state of nodes not configured in engine"""
        fw_id = 1
        fw_ids = self.wfe.fw_ids
        while fw_id in fw_ids:
            fw_id += 1
        with self.assertRaises(ConfigurationException):
            self.wfe.update_node(fw_id, {})
        with self.assertRaises(ConfigurationException):
            self.wfe.update_rerun_node(fw_id, {})
        with self.assertRaises(ConfigurationException):
            self.wfe.rerun_node(fw_id)
        with self.assertRaises(ConfigurationException):
            self.wfe.cancel_job(fw_id)

    def test_not_allowed_states(self):
        """test changing state of nodes in not allowed state"""
        with self.assertRaises(InvalidStateException):
            self.wfe.rerun_node(self.fw_id)
        with self.assertRaises(InvalidStateException):
            self.wfe.cancel_job(self.fw_id, restart=True)
        with self.assertRaises(InvalidStateException):
            self.wfe.update_rerun_node(self.fw_id, {})
        self.wfe.rlaunch(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'COMPLETED':
            raise TestingException('node is not in COMPLETED state')
        with self.assertRaises(InvalidStateException):
            self.wfe.update_node(self.fw_id, {})

    def test_cancel_missing_flag(self):
        """test to cancel a job missing restart or deactivate flag"""
        msg = 'Either restart or deactivate must be set to True'
        with self.assertRaises(ConfigurationException) as rexc:
            self.wfe.cancel_job(self.fw_id)
        self.assertIn(msg, str(rexc.exception))

    def test_rerun_paused_node(self):
        """test rerunning a paused node"""
        self.wfe.launchpad.pause_fw(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'PAUSED':
            raise TestingException('node is not in PAUSED state')
        self.wfe.rerun_node(self.fw_id)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'READY'

    def test_rerun_defused_node(self):
        """test rerunning a defused node"""
        self.wfe.launchpad.defuse_fw(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'DEFUSED':
            raise TestingException('node is not in DEFUSED state')
        self.wfe.rerun_node(self.fw_id)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'READY'

    def test_rerun_completed_node(self):
        """test rerunning a completed node"""
        self.wfe.rlaunch(self.fw_id)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'COMPLETED'
        self.wfe.rerun_node(self.fw_id)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'READY'

    def test_update_rerun_paused_node(self):
        """test update_rerun a paused node"""
        self.wfe.launchpad.pause_fw(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'PAUSED':
            raise TestingException('node is not in PAUSED state')
        update = {'name': str(uuid.uuid4())}
        self.wfe.update_rerun_node(self.fw_id, update)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'READY'
        assert self.launchpad.get_fw_by_id(self.fw_id).spec['name'] == update['name']

    def test_update_rerun_defused_node(self):
        """test update_rerun a defused node"""
        self.wfe.launchpad.defuse_fw(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'DEFUSED':
            raise TestingException('node is not in DEFUSED state')
        update = {'name': str(uuid.uuid4())}
        self.wfe.update_rerun_node(self.fw_id, update)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'READY'
        assert self.launchpad.get_fw_by_id(self.fw_id).spec['name'] == update['name']

    def test_update_rerun_completed_node(self):
        """test update_rerun a completed node"""
        self.wfe.rlaunch(self.fw_id)
        if self.launchpad.get_fw_by_id(self.fw_id).state != 'COMPLETED':
            raise TestingException('node is not in COMPLETED state')
        update = {'name': str(uuid.uuid4())}
        self.wfe.update_rerun_node(self.fw_id, update)
        assert self.launchpad.get_fw_by_id(self.fw_id).state == 'READY'
        assert self.launchpad.get_fw_by_id(self.fw_id).spec['name'] == update['name']

    def test_update_rerun_fizzled_node(self):
        """test update_rerun a fizzled node"""
        wf_name = str(uuid.uuid4())
        tasks = [PyTask(func='math.sqrt', args=[-1])]
        spec = {'_category': 'interactive'}
        workflow = Workflow([Firework(tasks=tasks, spec=spec)], name=wf_name)
        self.launchpad.add_wf(workflow)
        self.wfe.wf_query = {'name': wf_name}
        self.wfe.rlaunch(self.wfe.fw_ids[0])
        if self.launchpad.get_fw_by_id(self.wfe.fw_ids[0]).state != 'FIZZLED':
            raise TestingException('node is not in FIZZLED state')
        update = {'name': str(uuid.uuid4())}
        self.wfe.update_rerun_node(self.wfe.fw_ids[0], update)
        assert self.launchpad.get_fw_by_id(self.wfe.fw_ids[0]).state == 'READY'
        assert self.launchpad.get_fw_by_id(self.wfe.fw_ids[0]).spec['name'] == update['name']


class LauncherNodesTorunTest(ResConfigSetup):
    """test the launcher with a user-specified nodes to run"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.lpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.lpad = LaunchPad()
        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        spec = {'_category': 'interactive'}
        fws = []
        for ind in range(3):
            fws.append(Firework(tasks=ScriptTask.from_str('date'), spec=spec, name=str(ind)))
        links = {fws[0]: [fws[1], fws[2]], fws[1]: [], fws[2]: []}
        workflow = Workflow(fws, links_dict=links, name=wf_name, metadata={})
        self.lpad.add_wf(workflow)
        self.wf_ids = self.lpad.get_wf_ids(self.wf_query)
        self.wfe = WFEngine(launchpad=self.lpad, wf_query=self.wf_query,
                            sleep_time=1)

    def tearDown(self):
        if self.wfe.thread is not None and self.wfe.thread.is_alive():
            self.wfe.stop(join=True)
        self.lpad.delete_wf(self.wf_ids[0])
        super().tearDown()

    def test_nodes_torun_empty(self):
        """testing launcher with a custom empty list of nodes to run"""
        self.wfe.nodes_torun = []
        self.wfe.start()
        time.sleep(3)
        self.wfe.stop(join=True)
        fw_ids = self.lpad.get_fw_ids_in_wfs(self.wf_query, {'name': '1'})
        self.assertEqual(len(fw_ids), 1)
        self.assertEqual(self.lpad.get_fw_by_id(fw_ids[0]).state, 'WAITING')

    def test_nodes_torun(self):
        """testing launcher with a custom list of nodes to run"""
        fw_query = {'name': {'$in': ['0', '1']}}
        self.wfe.nodes_torun = self.lpad.get_fw_ids_in_wfs(self.wf_query, fw_query)
        self.wfe.start()
        time.sleep(3)
        self.wfe.stop(join=True)
        states = [self.lpad.get_fw_by_id(i).state for i in self.wfe.nodes_torun]
        self.assertTrue(all(s == 'COMPLETED' for s in states))
        fw_query = {'name': '2'}
        self.wfe.nodes_torun = self.lpad.get_fw_ids_in_wfs(self.wf_query, fw_query)
        states = [self.lpad.get_fw_by_id(i).state for i in self.wfe.nodes_torun]
        self.assertTrue(all(s == 'READY' for s in states))
        self.wfe.start()
        time.sleep(3)
        self.wfe.stop(join=True)
        states = [self.lpad.get_fw_by_id(i).state for i in self.wfe.nodes_torun]
        self.assertTrue(all(s == 'COMPLETED' for s in states))


@pytest.mark.slurm
class TestInconsistentNodes(ResConfigSetup):
    """test nodes with inconsistent states in FireWorks and SLURM"""
    def setUp(self):
        super().setUp()
        if LAUNCHPAD_LOC:
            self.launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.launchpad = LaunchPad()

        if QUEUEADAPTER_LOC:
            qadapter = CommonAdapter.from_file(QUEUEADAPTER_LOC)
        else:
            qadapter = CommonAdapter.from_dict(QADAPTER_DCT)
        self.qadapter_dct = qadapter.to_dict()

        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        tasks = [PyTask(func='time.sleep', inputs=['seconds'])]
        spec = {'seconds': 30, '_category': 'batch'}
        fwk = Firework(tasks=tasks, spec=spec, name='Sleep task')
        self.launchpad.add_wf(Workflow([fwk], links_dict={}, name=wf_name))
        self.wf_ids = self.launchpad.get_wf_ids(self.wf_query)
        nodes_query = {'nodes': {'$in': self.wf_ids}}
        self.fw_ids = self.launchpad.get_fw_ids_in_wfs(nodes_query)
        self.wfe = WFEngine(launchpad=self.launchpad, qadapter=qadapter,
                            wf_query=self.wf_query)

    def tearDown(self):
        self.launchpad.delete_wf(self.wf_ids[0])
        super().tearDown()

    @unittest.skip(reason='the test fails if job is immediately started')
    def test_get_unreserved_nodes_case_1(self):
        """test checking for unreserved nodes, case 1: reserved and
        not started within the passed 1 second"""
        fw_id = self.fw_ids[0]
        self.wfe.qlaunch(fw_id)
        res_id = self.launchpad.get_reservation_id_from_fw_id(fw_id)
        try:
            state = get_slurm_job_state(res_id, trial_delay=1)
        except SlurmError:
            pytest.skip('could not get SLURM job state')
        self.assertEqual(state, 'PENDING')
        unres = self.wfe.get_unreserved_nodes(time=1)
        self.assertEqual(len(unres), 1)
        self.assertEqual(unres[0]['fw_id'], fw_id)
        self.assertEqual(unres[0]['res_id'], res_id)
        self.assertEqual(unres[0]['slurm_state'], 'PENDING')
        self.assertTrue(isinstance(unres[0]['launch_dir'], str))
        self.assertNotEqual(len(unres[0]['launch_dir']), 0)

    def test_get_unreserved_nodes_case_2(self):
        """test checking for unreserved nodes, case 2: reserved but
        job has been cancelled in slurm"""
        fw_id = self.fw_ids[0]
        self.wfe.qlaunch(fw_id)
        res_id = self.launchpad.get_reservation_id_from_fw_id(fw_id)
        time.sleep(10)
        exec_cancel(res_id)
        try:
            state = get_slurm_job_state(res_id)
        except SlurmError:
            pytest.skip('could not get SLURM job state')
        self.assertEqual(state, 'CANCELLED')
        unres = self.wfe.get_unreserved_nodes(time=5)
        self.assertEqual(len(unres), 1)
        self.assertEqual(unres[0]['fw_id'], fw_id)
        self.assertEqual(unres[0]['res_id'], res_id)
        self.assertEqual(unres[0]['slurm_state'], 'CANCELLED')
        self.assertTrue(isinstance(unres[0]['launch_dir'], str))
        self.assertNotEqual(len(unres[0]['launch_dir']), 0)

    def test_get_unreserved_nodes_case_3(self):
        """test checking for unreserved nodes, case 3: started but
        crashed before rocket launcher was started"""
        fw_id = self.fw_ids[0]
        self.qadapter_dct['pre_rocket'] = 'set -e; ls ' + str(uuid.uuid4())
        self.wfe.qadapter = CommonAdapter.from_dict(self.qadapter_dct)
        self.wfe.qlaunch(fw_id)
        res_id = self.launchpad.get_reservation_id_from_fw_id(fw_id)
        time.sleep(10)
        try:
            await_slurm_job_state(res_id, 'FAILED')
        except SlurmError:
            pytest.skip('could not reach needed SLURM job state')
        unres = self.wfe.get_unreserved_nodes(time=5)
        self.assertEqual(len(unres), 1)
        self.assertEqual(unres[0]['fw_id'], fw_id)
        self.assertEqual(unres[0]['res_id'], res_id)
        self.assertEqual(unres[0]['slurm_state'], 'FAILED')
        self.assertTrue(isinstance(unres[0]['launch_dir'], str))
        self.assertNotEqual(len(unres[0]['launch_dir']), 0)

    def test_get_unreserved_nodes_case_4(self):
        """test checking for unreserved nodes, case 4: started but
        completed without the rocket launcher was started"""
        fw_id = self.fw_ids[0]
        self.qadapter_dct['pre_rocket'] = 'exit 0'
        self.wfe.qadapter = CommonAdapter.from_dict(self.qadapter_dct)
        self.wfe.qlaunch(fw_id)
        res_id = self.launchpad.get_reservation_id_from_fw_id(fw_id)
        time.sleep(15)
        try:
            await_slurm_job_state(res_id, 'COMPLETED')
        except SlurmError:
            pytest.skip('could not reach needed SLURM job state')
        unres = self.wfe.get_unreserved_nodes(time=10)
        self.assertEqual(len(unres), 1)
        self.assertEqual(unres[0]['fw_id'], fw_id)
        self.assertEqual(unres[0]['res_id'], res_id)
        self.assertEqual(unres[0]['slurm_state'], 'COMPLETED')
        self.assertTrue(isinstance(unres[0]['launch_dir'], str))
        self.assertNotEqual(len(unres[0]['launch_dir']), 0)

    @unittest.skip(reason='stuck in reserved state')
    def test_get_lost_jobs(self):
        """test checking for lost jobs when a SLURM job has been killed"""
        fw_id = self.fw_ids[0]
        self.wfe.qlaunch(fw_id)
        res_id = self.launchpad.get_reservation_id_from_fw_id(fw_id)
        await_slurm_job_state(res_id, 'RUNNING', sleep_time=5)
        time.sleep(15)
        assert self.launchpad.get_fw_by_id(fw_id).state == 'RUNNING'
        exec_cancel(res_id)
        try:
            state = get_slurm_job_state(res_id)
        except SlurmError:
            pytest.skip('could not get SLURM job state')
        self.assertEqual(state, 'CANCELLED')
        time.sleep(5)
        lost = self.wfe.get_lost_jobs(time=3)
        print(fw_id, self.fw_ids[0], res_id)
        self.assertEqual(len(lost), 1)
        self.assertEqual(lost[0], fw_id)


if __name__ == '__main__':
    unittest.main()
