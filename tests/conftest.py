"""configuration file for pytest, to better split slurm tests from general tests"""


def pytest_collection_modifyitems(items, config):
    """ add always_run to all non marked items"""
    for item in items:
        if not any(item.iter_markers()):
            item.add_marker("always_run")
    # Ensure the `always_run` marker is always selected for
    markexpr = config.getoption("markexpr", 'False')
    if len(markexpr) == 0:
        markexpr = "False"  # fix for getoption returning an empty string
    # now we can run all initially non marked tests and the -m tests if given
    config.option.markexpr = f"always_run or ({markexpr})"
