"""tests for the resource configuration"""
import os
import sys
import grp
import unittest
from tempfile import NamedTemporaryFile, mkdtemp
import pytest
from virtmat.middleware.resconfig import ResConfig, WorkerConfig, ModuleConfig
from virtmat.middleware.resconfig import QueueConfig, ResourceConfig, CommandConfig
from virtmat.middleware.resconfig import get_resconfig_loc, get_default_resconfig
from virtmat.middleware.resconfig import set_defaults_from_guess
from virtmat.middleware.resconfig import get_default_qadapter
from virtmat.middleware.resconfig import get_custom_qadapter
from virtmat.middleware.resconfig.resconfig import get_env_modules, get_venv
from virtmat.middleware.resconfig.qadapter import get_pre_rocket
from virtmat.middleware.exceptions import ResourceConfigurationError


@pytest.mark.slurm
class ResConfigTestWithSlurm(unittest.TestCase):
    """resconfig on a cluster with slurm"""
    def test_resconfig_bootstrap_from_scratch(self):
        """create a resconfig from scratch"""
        cfg1 = ResConfig.from_scratch(name='test_worker')
        cfg1.default_worker = cfg1.workers[0]
        self.assertEqual(cfg1.default_worker.name, 'test_worker')
        cfg1.default_worker.default_account = cfg1.default_worker.accounts[0]
        self.assertEqual(cfg1.default_worker.default_account, cfg1.default_worker.accounts[0])
        cfg2 = ResConfig.from_dict(cfg1.to_dict())
        self.assertEqual(cfg1, cfg2)

    def test_add_worker_config_from_scratch(self):
        """extend a resconfig with a worker config from scratch"""
        cfg = ResConfig()
        self.assertTrue(cfg.default_worker is None)
        cfg.add_worker_from_scratch(name='test_worker', default=True)
        self.assertTrue(cfg.default_worker is not None)
        cfg.add_worker_from_scratch(name='test_worker_2')
        self.assertEqual(cfg.default_worker, cfg.workers[0])
        self.assertEqual(cfg.default_worker.name, 'test_worker')
        self.assertEqual(cfg.workers[1].name, 'test_worker_2')

    def test_create_resconfig_with_custom_worker_config_from_scratch(self):
        """create a resconfig with custom worker config from scratch"""
        worker = WorkerConfig.from_scratch()
        cfg = ResConfig(workers=[worker], default_worker=worker)
        self.assertEqual(cfg.default_worker, cfg.workers[0])


@pytest.mark.slurm
class WorkerConfigTestWithSlurm(unittest.TestCase):
    """worker config on a cluster with slurm"""
    def test_worker_with_set_default_account(self):
        """create a worker add set a default account"""
        cfg = WorkerConfig.from_scratch()
        cfg.set_default_account()
        self.assertEqual(cfg.default_account, grp.getgrgid(os.getgid()).gr_name)


class ResConfigTestWithoutSlurm(unittest.TestCase):
    """test resconfig on a cluster without slurm"""

    @unittest.skip(reason='test can only pass if slurm is not available')
    def test_resconfig_bootstrap_from_scratch(self):
        """create a configuration from scratch"""
        with self.assertRaises(FileNotFoundError):
            ResConfig.from_scratch()

    def test_create_resconfig_with_no_workers(self):
        """create a resconfig with no workers"""
        cfg = ResConfig()
        self.assertEqual(len(cfg.workers), 0)

    def test_create_resconfig_with_custom_worker_config(self):
        """create a resconfig with custom worker config"""
        worker = WorkerConfig()
        cfg = ResConfig(workers=[worker], default_worker=worker)
        self.assertEqual(cfg.default_worker, cfg.workers[0])


class WorkerConfigTest(unittest.TestCase):
    """test worker configuration"""
    def test_worker_config(self):
        """create an empty named worker configuration"""
        cfg = WorkerConfig(name='test_worker')
        self.assertEqual(cfg.name, 'test_worker')
        self.assertEqual(len(cfg.queues), 0)

    def test_worker_with_default_queue(self):
        """create a worker, add a queue and make it default"""
        queue = QueueConfig(public=True, name='test_q')
        cfg = WorkerConfig(queues=[queue])
        cfg.default_queue = queue
        self.assertTrue(cfg.default_queue.public)
        self.assertEqual(cfg.default_queue.name, 'test_q')

    def test_worker_with_set_default_queue(self):
        """create a worker, add two queues and make a default queue"""
        queue1 = QueueConfig(public=True, name='test_q1')
        queue2 = QueueConfig(public=False, name='test_q2')
        cfg = WorkerConfig(queues=[queue1, queue2])
        cfg.set_default_queue()
        self.assertTrue(cfg.default_queue.public)
        self.assertEqual(cfg.default_queue.name, 'test_q1')

    def test_worker_default_launchdir(self):
        """create a worker configuration with default launchdir"""
        launchdir = mkdtemp()
        cfg = WorkerConfig(name='test_worker', default_launchdir=launchdir)
        self.assertEqual(cfg.default_launchdir, launchdir)
        os.rmdir(launchdir)


class QueueConfigTest(unittest.TestCase):
    """test queue configuration"""
    def test_queue_config_no_resources(self):
        """create a public named queue configuration"""
        cfg = QueueConfig(public=True, name='test_queue')
        self.assertEqual(cfg.name, 'test_queue')
        self.assertTrue(cfg.public)
        self.assertEqual(len(cfg.resources), 0)
        cfg.resources.append(ResourceConfig(name='rs_name'))
        cfg.set_resource('rs_name', 'minimum', 1)
        self.assertEqual(cfg.resources[0].minimum, 1)
        self.assertEqual(cfg.get_resource('rs_name').minimum, 1)
        cfg.set_resource('rs_name', 'default', 2)
        self.assertEqual(cfg.resources[0].default, 2)
        self.assertEqual(cfg.get_resource('rs_name').default, 2)

    def test_queue_config_validate_resource(self):
        """test validate_resource() method"""
        cfg = QueueConfig(name='test_q')
        cfg.set_resource('r_name', 'minimum', 5)
        cfg.set_resource('r_name', 'maximum', 10)
        cfg.validate_resource('r_name', 6)
        with self.assertRaises(ResourceConfigurationError):
            cfg.validate_resource('r_name', 4)
        with self.assertRaises(ResourceConfigurationError):
            cfg.validate_resource('r_name', 12)


class ResourceConfigTest(unittest.TestCase):
    """test queue resource configuration"""
    def test_resource_config(self):
        """resource with no value"""
        cfg = ResourceConfig(name='rs_name')
        self.assertTrue(cfg.minimum is None)
        cfg.minimum = 2
        self.assertEqual(cfg.minimum, 2)
        self.assertTrue(cfg.default is None)
        cfg.default = 2
        self.assertEqual(cfg.default, 2)


class ResConfigUtilityFunctionsTest(unittest.TestCase):
    """test utility functions in reconfig module"""

    def test_set_defaults_from_guess(self):
        """test set_defaults_from_guess function"""
        my_account = grp.getgrgid(os.getgid()).gr_name
        cfg = WorkerConfig(queues=[QueueConfig()], accounts=[my_account])
        set_defaults_from_guess(cfg)
        self.assertTrue(cfg.name is not None)
        self.assertTrue(cfg.default_queue is not None)
        self.assertEqual(cfg.default_queue.get_resource('time').default, 5)
        self.assertEqual(cfg.default_queue.get_resource('nodes').default, 1)
        self.assertEqual(cfg.default_queue.get_resource('cpus_per_task').default, 1)
        self.assertEqual(cfg.default_queue.get_resource('cpus_per_node').default, 1)

    def test_get_resconfig_loc(self):
        """test get_resconfig_loc() function"""
        loc = os.path.join(os.path.expanduser('~'), '.fireworks', 'res_config.yaml')
        self.assertEqual(get_resconfig_loc(), loc)


class GetDefaultResconfigTest(unittest.TestCase):
    """test get_default_resconfig() utility function in reconfig module"""

    @pytest.fixture(autouse=True)
    def set_resconfig_loc_env(self, monkeypatch):
        """create an environment with RESCONFIG_LOC set"""
        monkeypatch.setenv('RESCONFIG_LOC', '/wrong/path/to/reconfig')

    def test_get_default_resconfig(self):
        """test get_default_resconfig() function"""
        self.assertEqual(get_default_resconfig(), None)


class QadapterTest(unittest.TestCase):
    """test the qadapter utility functions"""

    def setUp(self):
        self.queue = QueueConfig(name='test_q')
        self.account = grp.getgrgid(os.getgid()).gr_name
        self.mcfg = ModuleConfig(prefix='chem', name='gromacs', versions=['2022.5', '2022.6'])
        self.wcfg = WorkerConfig(queues=[self.queue], type_='SLURM', accounts=[self.account],
                                 modules=[self.mcfg])
        self.wcfg.name = 'test_w'
        self.cfg = ResConfig(workers=[self.wcfg], default_worker=self.wcfg)
        set_defaults_from_guess(self.cfg.default_worker)
        with NamedTemporaryFile(mode='w', prefix='launchpad-', suffix='.yaml',
                                encoding='utf-8', delete=False) as fh:
            self.lp_file = fh.name

    def tearDown(self):
        os.unlink(self.lp_file)

    def test_get_default_qadapter(self):
        """test the function creating a default qadapter"""
        qadapter = get_default_qadapter(self.cfg)
        self.assertEqual(qadapter.q_name, 'test_q')
        self.assertEqual(qadapter['walltime'], 5)
        self.assertEqual(qadapter['nodes'], 1)
        self.assertEqual(qadapter['ntasks'], 1)
        self.assertEqual(qadapter['ntasks_per_node'], 1)
        self.assertEqual(qadapter['cpus_per_task'], 1)
        self.assertEqual(qadapter['account'], self.account)

    def test_get_default_qadapter_custom_launchpad(self):
        """test the function creating a default qadapter with a custom launchpad"""
        qadapter = get_default_qadapter(self.cfg, lp_file=self.lp_file)
        self.assertTrue(self.lp_file in qadapter['rocket_launch'])

    def test_get_default_qadapter_launchdir(self):
        """test the function creating a default qadapter with launchdir"""
        launchdir = mkdtemp()
        self.wcfg.default_launchdir = launchdir
        qadapter = get_default_qadapter(self.cfg)
        self.assertEqual(qadapter['launch_dir'], launchdir)
        os.rmdir(launchdir)

    def test_get_custom_qadapter_known_worker(self):
        """test the function creating a custom qadapter for a selected worker"""
        res = {'ntasks_per_node': 20, 'cpus_per_task': 2, 'walltime': 10}
        w_name, qadapter = get_custom_qadapter(self.cfg, self.wcfg.name, self.queue.name, **res)
        self.assertEqual(w_name, 'test_w')
        self.assertEqual(qadapter.q_name, 'test_q')
        self.assertEqual(qadapter['walltime'], 10)
        self.assertEqual(qadapter['nodes'], 1)
        self.assertEqual(qadapter['ntasks'], 20)
        self.assertEqual(qadapter['ntasks_per_node'], 20)
        self.assertEqual(qadapter['cpus_per_task'], 2)
        self.assertEqual(qadapter['account'], self.account)

    def test_get_custom_qadapter_unknown_worker(self):
        """test the function creating a custom qadapter for any worker"""
        res = {'ntasks_per_node': 20, 'cpus_per_task': 2, 'walltime': 10}
        w_name, qadapter = get_custom_qadapter(self.cfg, **res)
        self.assertEqual(w_name, 'test_w')
        self.assertEqual(qadapter.q_name, 'test_q')
        self.assertEqual(qadapter['walltime'], 10)
        self.assertEqual(qadapter['nodes'], 1)
        self.assertEqual(qadapter['ntasks'], 20)
        self.assertEqual(qadapter['ntasks_per_node'], 20)
        self.assertEqual(qadapter['cpus_per_task'], 2)
        self.assertEqual(qadapter['account'], self.account)

    def test_get_custom_qadapter_custom_launchpad(self):
        """test the function creating a custom qadapter with a custom launchpad"""
        _, qadapter = get_custom_qadapter(self.cfg, lp_file=self.lp_file)
        self.assertTrue(self.lp_file in qadapter['rocket_launch'])

    def test_get_custom_qadapter_launchdir(self):
        """test the function creating a custom qadapter with launchdir"""
        launchdir = mkdtemp()
        self.wcfg.default_launchdir = launchdir
        _, qadapter = get_custom_qadapter(self.cfg)
        self.assertEqual(qadapter['launch_dir'], launchdir)
        os.rmdir(launchdir)

    def test_get_custom_qadapter_module_version(self):
        """test get_custom_qadapter with module version"""
        _, qadapter = get_custom_qadapter(self.cfg, modules={'gromacs': '==2022.6'})
        self.assertIn('module load chem/gromacs/2022.6', qadapter['pre_rocket'])

    def test_get_custom_qadapter_module_no_version(self):
        """test get_custom_qadapter() with module no version specification"""
        _, qadapter = get_custom_qadapter(self.cfg, modules={'gromacs': None})
        self.assertIn('module load chem/gromacs/2022.6', qadapter['pre_rocket'])

    def test_get_custom_qadapter_module_version_not_avail(self):
        """test get_custom_qadapter() with module no available version"""
        _, qadapter = get_custom_qadapter(self.cfg, modules={'gromacs': '>2022.6'})
        self.assertTrue(qadapter is None)

    def test_get_custom_qadapter_env_variable(self):
        """test get_custom_qadapter() with env variable"""
        _, qadapter = get_custom_qadapter(self.cfg, envs={'ENVNAME': 'env_value'})
        self.assertEqual(qadapter['pre_rocket'], 'module purge; export ENVNAME=env_value')

    def test_get_custom_qadapter_unset_env_variable(self):
        """test get_custom_qadapter() with env variable to be unset"""
        _, qadapter = get_custom_qadapter(self.cfg, envs={'ENVNAME': None})
        self.assertEqual(qadapter['pre_rocket'], 'module purge; unset ENVNAME')


class LmodTest(unittest.TestCase):
    """test the get_env_modules() utility function"""

    @pytest.fixture(autouse=True)
    def delenv_lmod_cmd(self, monkeypatch):
        """create an environment without lmod modules"""
        monkeypatch.delenv('LMOD_CMD', raising=False)

    def test_get_env_modules(self):
        """test get_env_modules()"""
        self.assertEqual(get_env_modules('loaded'), [])
        self.assertEqual(get_env_modules('available'), [])


class CondaTest(unittest.TestCase):
    """test get_venv() utility function in conda virtual environment"""

    @pytest.fixture(autouse=True)
    def setup_conda_venv(self, monkeypatch):
        """create a conda virtual environment"""
        monkeypatch.setenv('CONDA_DEFAULT_ENV', 'default')
        monkeypatch.setenv('CONDA_PREFIX', 'prefix')

    def test_conda_venv(self):
        """test in conda virtual environment"""
        ref = {'type': 'conda', 'name': 'default', 'prefix': 'prefix'}
        self.assertEqual(get_venv(), ref)


class VenvTest(unittest.TestCase):
    """test get_venv() utility function in venv virtual environment"""

    @pytest.fixture(autouse=True)
    def setup_venv(self, monkeypatch):
        """create a venv virtual environment"""
        monkeypatch.setattr(sys, 'prefix', '/path/to/default')

    def test_venv(self):
        """test in venv virtual environment"""
        ref = {'type': 'venv', 'name': 'default', 'prefix': '/path/to/default'}
        self.assertEqual(get_venv(), ref)


class NoVenvTest(unittest.TestCase):
    """test get_venv() utility function without virtual environment"""

    @pytest.fixture(autouse=True)
    def setup_no_venv(self, monkeypatch):
        """create an environment without venv"""
        monkeypatch.delenv('CONDA_DEFAULT_ENV', raising=False)
        monkeypatch.delenv('CONDA_PREFIX', raising=False)
        monkeypatch.setattr(sys, 'prefix', sys.base_prefix)

    def test_no_venv(self):
        """test without virtual environment"""
        self.assertTrue(get_venv() is None)


class PreRocketTest(unittest.TestCase):
    """test get_pre_rocket() utility function"""

    def setUp(self):
        mods = ModuleConfig(prefix='chem', name='gromacs', versions=['2022.5'])
        def_mods = ModuleConfig(prefix='devel', name='python', versions=['3.10.5_gnu_12.1'])
        venv = {'type': 'venv', 'name': 'default', 'prefix': '/path/to/default'}
        self.wcfg = WorkerConfig(modules=[mods], default_modules=[def_mods],
                                 default_venv=venv)

    def test_default_pre_rocket(self):
        """test the default pre_rocket with no requested modules or envvars"""
        ref = ('module purge; module load devel/python/3.10.5_gnu_12.1; source'
               ' /path/to/default/bin/activate')
        self.assertEqual(get_pre_rocket(self.wcfg), ref)

    def test_custom_pre_rocket(self):
        """test the default pre_rocket with request for modules or envvars"""
        req = {'modules': {'gromacs': '>2022.0'}, 'envs': {'ENV': 'envval'}}
        ref = ('module purge; module load devel/python/3.10.5_gnu_12.1; '
               'module load chem/gromacs/2022.5; source /path/to/default/bin/activate; '
               'export ENV=envval')
        self.assertEqual(get_pre_rocket(self.wcfg, **req), ref)


class PreRocketTestEnvvars(unittest.TestCase):
    """test get_pre_rocket() utility function with envvars"""

    def setUp(self):
        self.wcfg = WorkerConfig(envvars={'ENV2': 'env2_val', 'ENV3': 3},
                                 default_envvars={'ENV3': 3})

    def test_default_pre_rocket_envvars(self):
        """test the default pre_rocket with envvars"""
        ref = 'export ENV3=3'
        self.assertEqual(get_pre_rocket(self.wcfg), ref)

    def test_custom_pre_rocket_envvars(self):
        """test the custom pre_rocket with envvars"""
        req = {'envs': {'ENV2': None}}
        ref = 'export ENV3=3; export ENV2=env2_val'
        self.assertEqual(get_pre_rocket(self.wcfg, **req), ref)

    def test_default_pre_rocket_envvars_exception(self):
        """test the default pre_rocket with envvars and exception"""
        self.wcfg.envvars = {}
        with self.assertRaises(ResourceConfigurationError):
            get_pre_rocket(self.wcfg)


class PreRocketTestCommands(unittest.TestCase):
    """test get_pre_rocket() utility function with default shell commands"""

    def setUp(self):
        cmds = [CommandConfig(cmd='umask', args=['027'])]
        self.wcfg = WorkerConfig(default_commands=cmds)

    def test_default_pre_rocket_envvars(self):
        """test the default pre_rocket with default shell command"""
        ref = 'umask 027'
        self.assertEqual(get_pre_rocket(self.wcfg), ref)

    def test_default_pre_rocket_envvars_exception(self):
        """test the default pre_rocket with default shell command with exception"""
        with self.assertRaises(ResourceConfigurationError):
            self.wcfg.default_commands.append(CommandConfig(cmd='echo'))
