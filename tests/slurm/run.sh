#!/bin/bash -eu

VENV_LOCATION=$HOME/ci/civenv
VENV_ACTIVATE=$VENV_LOCATION/bin/activate

if [[ ! -f "$VENV_ACTIVATE" ]]; then
    python3.9 -m venv $VENV_LOCATION
fi

. $VENV_ACTIVATE
python -m pip install --upgrade pip
python -m pip install -r requirements_test.txt --quiet
python -m pip install . --quiet
ln -s $HOME/ci/fireworks_ci/FW_config.yaml
python tests/slurm/execute.py
