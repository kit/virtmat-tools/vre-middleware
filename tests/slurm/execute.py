"""This script is specific for HoreKa and bwUniCluster"""
import socket
import time
import os
import sys
import pathlib
from subprocess import Popen
from datetime import datetime
import requests
import pytz
import yaml
from dateutil import parser

CI_RUN_DIR = 'ci'
FW_CI_CONFIG_DIR = os.path.join(CI_RUN_DIR, 'fireworks_ci')
DOCKER_URL = 'docker://registry.gitlab.com#ikondov/vre-ci-docker/ci-docker'
DOCKER_CONTAINER = ('https://gitlab.com/api/v4/projects/ikondov%2Fvre-ci-docker'
                    '/registry/repositories/3466225/tags/latest')


def run_cmd(cmd):
    """run command in subprocess, and return the exitcode"""
    print('run: ', cmd)
    with Popen(cmd.split()) as run:  # nosec B603
        run.communicate()
    return run.returncode


def import_create_image(path_home):
    """
    check that the most recent docker image is available on the cluster,
    otherwise download and create it
    """
    no_local = False
    path_sqsh = os.path.join(path_home, CI_RUN_DIR, 'vre-ci-docker.sqsh')
    response = requests.get(DOCKER_CONTAINER, timeout=60)
    time_remote = parser.parse(response.json()['created_at'])
    print('Container was created at: ', time_remote.isoformat())

    try:
        time_local = datetime.utcfromtimestamp(os.path.getmtime(path_sqsh)).replace(tzinfo=pytz.UTC)
        print('Local sqsh representation was last modified at: ', time_local.isoformat())
    except OSError:
        print('No local representation found')
        no_local = True

    if no_local or time_remote > time_local:
        print('Local representation is old, redownloading and recreating...')
        if not no_local:
            print('remove ', path_sqsh)
            os.remove(path_sqsh)
            run_cmd('enroot remove -f mongodb')

        os.chdir(os.path.join(path_home, CI_RUN_DIR))
        run_cmd('enroot import -o '+path_sqsh+' '+DOCKER_URL)
        run_cmd('enroot create --name mongodb '+path_sqsh)
    else:
        print('Using local representation')


def setup_files(path_home):
    """
    setup the fireworks files needed for the test, while preserving
    existing configurations
    """
    hostname = socket.gethostname()
    fw_config_dir = os.path.join(path_home, FW_CI_CONFIG_DIR)
    if not os.path.exists(fw_config_dir):
        os.mkdir(fw_config_dir)

    fw_config_file = os.path.join(fw_config_dir, 'FW_config.yaml')
    qadapter_file = os.path.join(fw_config_dir, 'ci_qadapter.yaml')
    lpad_file = os.path.join(fw_config_dir, 'ci_launchpad.yaml')

    fw_config = dict(LAUNCHPAD_LOC=lpad_file, QUEUEADAPTER_LOC=qadapter_file)
    with open(fw_config_file, 'w', encoding='utf-8') as cfile:
        yaml.safe_dump(fw_config, cfile)

    lpad_config = dict(host='mongodb://'+hostname, port=27017)
    with open(lpad_file, 'w', encoding='utf-8') as lpfile:
        yaml.safe_dump(lpad_config, lpfile)

    rlaunch_str = 'rlaunch -l ' + lpad_file + ' singleshot'
    qadapter = dict(_fw_name='CommonAdapter', _fw_q_type='SLURM',
                    rocket_launch=rlaunch_str, nodes=1, ntasks=1,
                    walltime='00:01:00')

    with open(qadapter_file, 'w', encoding='utf-8') as qfile:
        if "hkn" in hostname:
            qadapter['queue'] = 'dev_cpuonly'
            qadapter['account'] = 'hk-project-consulting'
        elif "uc2n" in hostname:
            qadapter['queue'] = 'dev_single'
        yaml.safe_dump(qadapter, qfile)


def reset_files(path_home):
    """remove the ci configuration if it exists"""
    fw_config_dir = os.path.join(path_home, FW_CI_CONFIG_DIR)
    fw_config_file = os.path.join(fw_config_dir, 'FW_config.yaml')
    qadapter_file = os.path.join(fw_config_dir, 'ci_qadapter.yaml')
    lpad_file = os.path.join(fw_config_dir, 'ci_launchpad.yaml')
    if os.path.isfile(fw_config_file):
        os.remove(fw_config_file)
    if os.path.isfile(lpad_file):
        os.remove(lpad_file)
    if os.path.isfile(qadapter_file):
        os.remove(qadapter_file)


def main():
    """start WFEngineTest with the correct environment to test slurm"""
    path_home = str(pathlib.Path.home())

    reset_files(path_home)  # reset files before creating new ones
    setup_files(path_home)
    import_create_image(path_home)

    # start mongodb on ci-node
    with open(os.devnull, 'w', encoding='utf-8') as devnull:
        # B603 is no problem, as no user input is passed to Popen
        command = ['/usr/bin/enroot', 'start', '--rw', 'mongodb']
        with Popen(command, stdout=devnull) as proc:  # nosec B603
            time.sleep(1)
            proc.poll()  # check if mongodb started after 1 second
            print(proc.returncode)
            if proc.returncode == 1:
                print('could not start mongodb')
                sys.exit(1)
            else:
                print('mongodb launched with pid: ', proc.pid)

            time.sleep(2)
            # run_cmd / subprocess had some problems with the pipe, therefore
            # we still have os.system here (B605)
            # the path for lpad is in a python venv, in different user paths,
            # therfore we can't specify the path (B607)
            # this should still be good to be executed within the ci environment
            os.system('echo "Y" | lpad reset')  # nosec B605, B607
            pytest_return = run_cmd('pytest -v -m slurm')

            time.sleep(2)
            proc.kill()

    sys.exit(pytest_return)  # return the pytest return value


if __name__ == '__main__':
    main()
