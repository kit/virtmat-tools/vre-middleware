"""test the command-line interface tools"""
import os
import json
import unittest
from tempfile import NamedTemporaryFile
from fireworks import LaunchPad, PyTask, Firework, Workflow
from fireworks.fw_config import LAUNCHPAD_LOC
from virtmat.middleware.cli import run_wfengine, run_resconfig
from virtmat.middleware.exceptions import ConfigurationException
from .wfengine_test import ResConfigSetup


class RunEngineBaseTest(unittest.TestCase):
    """test the wfengine script"""

    def setUp(self):
        self.launchpad_file = None
        self.qadapter_file = None
        self.worker_name = None
        self.launchdir = None
        self.sleep_time = 2
        self.category = 'all'
        self.workflow_file = None
        self.autorun = False
        self.wf_query = None
        self.unique_launchdir = True
        self.loop = True
        self.load_from_file = None
        self.dump_to_file = None
        self.enable_logging = False
        self.logging_level = 'CRITICAL'


class RunEngineMainTest(RunEngineBaseTest):
    """test the wfengine script"""

    def test_run_wfengine_main_no_launchpad(self):
        """test wfengine with no launchpad"""
        msg = 'no launchpad could be detected'
        with self.assertRaisesRegex(ConfigurationException, msg):
            run_wfengine.wfengine_main(self)

    def test_run_main(self):
        """test the main function used as entry point"""
        try:
            run_wfengine.main()
        except (SystemExit, ConfigurationException):
            pass


class RunEngineDefaultTest(ResConfigSetup, RunEngineBaseTest):
    """test the wfengine script"""

    def setUp(self):
        ResConfigSetup.setUp(self)
        RunEngineBaseTest.setUp(self)
        lpad = LaunchPad.from_file(LAUNCHPAD_LOC) if LAUNCHPAD_LOC else LaunchPad()
        with NamedTemporaryFile(mode='w', prefix='launchpad-', suffix='.yaml',
                                encoding='utf-8', delete=False) as fh:
            self.launchpad_file = fh.name
            lpad.to_file(self.launchpad_file)

    def tearDown(self):
        os.unlink(self.launchpad_file)
        RunEngineBaseTest.tearDown(self)
        ResConfigSetup.tearDown(self)


class RunEngineDefaultMainTest(RunEngineDefaultTest):
    """test the wfengine script"""

    def test_run_wfengine_main_default(self):
        """test the main function called with default clargs"""
        run_wfengine.wfengine_main(self)


class RunEngineDumpTest(RunEngineDefaultTest):
    """test the wfengine script"""

    def setUp(self):
        super().setUp()
        with NamedTemporaryFile(mode='w', prefix='wfengine-', suffix='.yaml',
                                encoding='utf-8', delete=False) as fh:
            self.dump_to_file = fh.name

    def tearDown(self):
        os.unlink(self.dump_to_file)
        super().tearDown()


class RunEngineDumpMainTest(RunEngineDumpTest):
    """test the wfengine script"""

    def test_dump_wfengine_to_file(self):
        """test dumping a wfengine to a file"""
        run_wfengine.wfengine_main(self)


class RunEngineLoadMainTest(RunEngineDumpTest):
    """test the wfengine script"""

    def setUp(self):
        super().setUp()
        run_wfengine.wfengine_main(self)
        self.load_from_file = self.dump_to_file
        self.wf_query = '{}'

    def test_load_wfengine_from_file(self):
        """test loading wfengine from a file"""
        run_wfengine.wfengine_main(self)


class RunEngineWorkflowTest(RunEngineDefaultTest):
    """test the wfengine script"""

    def setUp(self):
        super().setUp()
        tasks = [PyTask(func='math.pow', inputs=['source', 'power'], outputs=['number'])]
        spec = {'source': 2, 'power': 2, '_category': 'interactive'}
        fw = Firework(tasks=tasks, spec=spec)
        with NamedTemporaryFile(mode='w', prefix='workflow-', suffix='.yaml',
                                encoding='utf-8', delete=False) as fh:
            self.workflow_file = fh.name
        workflow = Workflow([fw], links_dict={}, name=self.workflow_file, metadata={})
        workflow.to_file(self.workflow_file)
        self.wf_query = json.dumps({'name': self.workflow_file})
        self.autorun = True

    def test_run_wfengine_no_loop(self):
        """test running workflow in an engine"""
        self.loop = False
        run_wfengine.wfengine_main(self)


class RunResconfigTest(ResConfigSetup):
    """run tests for the resconfig CLI tool"""

    def setUp(self):
        super().setUp()
        self.interactive = False
        self.overwrite = False
        self.path = None
        with NamedTemporaryFile(mode='w', prefix='resconfig-', suffix='.yaml',
                                encoding='utf-8', delete=False) as fh:
            self.alt_path = fh.name

    def tearDown(self):
        os.unlink(self.alt_path)
        super().tearDown()

    def test_run_resconfig_main(self):
        """run test of the main function / entry point"""
        try:
            run_resconfig.main()
        except (SystemExit, ConfigurationException):
            pass

    def test_run_resconfig_default(self):
        """run test with default settings"""
        run_resconfig.resconfig_main(self)

    def test_run_resconfig_interactive(self):
        """run test with the interactive flag"""
        self.interactive = True
        run_resconfig.resconfig_main(self)

    def test_run_resconfig_overwrite(self):
        """run test with the overwrite flag"""
        self.overwrite = True
        run_resconfig.resconfig_main(self)

    def test_run_resconfig_path(self):
        """run test with a alternative path"""
        self.overwrite = True
        self.path = self.alt_path
        run_resconfig.resconfig_main(self)


if __name__ == '__main__':
    unittest.main()
