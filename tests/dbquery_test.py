""" Unit tests for the dbquery module """
import uuid
import unittest
from fireworks.fw_config import LAUNCHPAD_LOC
from fireworks.core.launchpad import LaunchPad
from fireworks import Firework, Workflow, PyTask
from virtmat.middleware.query.dbquery import db_select
from virtmat.middleware.exceptions import ConfigurationException


class DBQueryTest(unittest.TestCase):
    """ test the db_select function """

    def setUp(self):
        if LAUNCHPAD_LOC:
            self.lpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.lpad = LaunchPad()
        # assert self.lpad.name == 'virtmat-vre-unittest'

        self.wf_name = str(uuid.uuid4())
        self.wf_query = {'name': self.wf_name}
        tasks = [PyTask(func='math.pow', inputs=['source', 'power'],
                        outputs=['number'])]
        spec = {'source': 2, 'power': 2}
        fw_1 = Firework(tasks=tasks, spec=spec, name='Power task')
        tasks = [PyTask(func='math.sqrt', inputs=['number'])]
        spec = {}
        fw_2 = Firework(tasks=tasks, spec=spec, name='Square root task')
        fws = [fw_1, fw_2]
        links = {fws[0]: [fws[1]], fws[1]: []}
        workflow = Workflow(fws, links_dict=links, name=self.wf_name)
        self.lpad.add_wf(workflow)
        self.wf_ids = self.lpad.get_wf_ids(self.wf_query)
        if len(self.wf_ids) != 1:
            raise ConfigurationException("wf_ids has length != 1")

    def tearDown(self):
        self.lpad.delete_wf(self.wf_ids[0])

    def test_dbquery_wf(self):
        """ test a workflow query """

        filters = {'workflows': {'name': self.wf_name, 'state': 'READY'}}
        docs = db_select(self.lpad, filters=filters, selects=[])
        self.assertEqual(len(docs), 1)
        self.assertIn('fws', docs[0])
        self.assertEqual(len(docs[0]['fws']), 0)

    def test_dbquery_fw(self):
        """ test a fireworks query """

        filters = {'fireworks': {'name': 'Square root task'}}
        docs = db_select(self.lpad, filters=filters)
        self.assertEqual(len(docs), 1)
        self.assertEqual(docs[0]['name'], self.wf_name)
        self.assertIn('fws', docs[0])
        self.assertEqual(len(docs[0]['fws']), 0)

    def test_dbquery_id(self):
        """ test a firework id query """

        docs = db_select(self.lpad, ids=self.wf_ids)
        self.assertEqual(len(docs), 1)
        self.assertEqual(docs[0]['name'], self.wf_name)
        self.assertIn('fws', docs[0])
        self.assertEqual(len(docs[0]['fws']), 0)

    def test_dbquery_id_projection(self):
        """ test a firework id query with projection """

        selects = [{'fw_name': 'Power task', 'add fw_spec': True,
                    'fw_updates': 'number'},
                   {'fw_name': 'Square root task',
                    'add fw_spec': False, 'fw_updates': True}]
        docs = db_select(self.lpad, ids=self.wf_ids, selects=selects)
        self.assertEqual(len(docs), 1)
        self.assertEqual(docs[0]['name'], self.wf_name)
        self.assertIn('fws', docs[0])
        self.assertEqual(len(docs[0]['fws']), 2)
        self.assertEqual(docs[0]['fws'][0]['name'], 'Power task')
        self.assertEqual(docs[0]['fws'][1]['name'], 'Square root task')
        self.assertIn('spec', docs[0]['fws'][0])
        self.assertIn('source', docs[0]['fws'][0]['spec'])
        self.assertNotIn('spec', docs[0]['fws'][1])
        self.assertEqual(docs[0]['fws'][0]['spec']['source'], 2)
        self.assertNotEqual(docs[0]['fws'][1]['parents'], None)
        self.assertEqual(len(docs[0]['fws'][1]['parents']), 1)
        self.assertIn(docs[0]['fws'][0]['id'], docs[0]['fws'][1]['parents'])
        self.assertEqual(docs[0]['fws'][0]['updates'], None)
        self.assertEqual(docs[0]['fws'][1]['updates'], None)


if __name__ == '__main__':
    unittest.main()
