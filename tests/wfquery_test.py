""" Unit tests for the wfquery module """
import uuid
import unittest
from fireworks.fw_config import LAUNCHPAD_LOC
from fireworks.core.launchpad import LaunchPad
from fireworks import Firework, Workflow, PyTask
from virtmat.middleware.exceptions import ConfigurationException, TestingException
from virtmat.middleware.query.wfquery import WFQuery


class WFQueryTest(unittest.TestCase):
    """ test the WFQuery class """

    def setUp(self):
        if LAUNCHPAD_LOC:
            self.lpad = LaunchPad.from_file(LAUNCHPAD_LOC)
        else:
            self.lpad = LaunchPad()

        wf_name = str(uuid.uuid4())
        self.wf_query = {'name': wf_name}
        tasks = [PyTask(func='math.pow', inputs=['source', 'power'],
                        outputs=['number'])]
        spec = {'source': 2, 'power': 2}
        fw_1 = Firework(tasks=tasks, spec=spec, name='Power task')
        tasks = [PyTask(func='math.sqrt', inputs=['number'])]
        spec = {}
        fw_2 = Firework(tasks=tasks, spec=spec, name='Square root task')
        fws = [fw_1, fw_2]
        links = {fws[0]: [fws[1]], fws[1]: []}
        workflow = Workflow(fws, links_dict=links, name=wf_name, metadata={})
        self.lpad.add_wf(workflow)
        self.wf_ids = self.lpad.get_wf_ids(self.wf_query)
        self.fw_ids = self.lpad.get_fw_ids_in_wfs(self.wf_query)
        if len(self.wf_ids) != 1:
            raise TestingException("wf_ids has length != 1")

    def tearDown(self):
        self.lpad.delete_wf(self.wf_ids[0])

    def test_wf(self):
        """ test a workflow query on the whole workflow """
        qobj = WFQuery(self.lpad, self.wf_query)
        print(qobj.get_wf_info())
        print(qobj.get_wf_info(add_io_info=False))  # no inputs/outputs in table
        self.assertEqual(len(qobj.get_wf_ids()), 1)
        for fw_id in qobj.get_wf_ids():
            self.assertIn(fw_id, self.fw_ids)
        self.assertEqual(len(qobj.get_data('power', 'input')), 1)
        self.assertEqual(len(qobj.get_data('number', 'input')), 0)
        self.assertEqual(len(qobj.get_data('number', 'output')), 0)
        self.assertEqual(len(qobj.check_fw_ids()), 2)
        self.assertEqual(qobj.check_fw_ids(self.wf_ids), self.wf_ids)
        with self.assertRaises(ConfigurationException) as context:
            qobj.check_fw_ids([-1])
        self.assertTrue('invalid fw_ids' in str(context.exception))

    def test_fw_ids(self):
        """ test firework queries firework-wise """
        fw_query = {'name': 'Power task'}
        qobj = WFQuery(self.lpad, self.wf_query, fw_query, metadata_only=True)
        self.assertEqual(len(qobj.get_fw_ids()), 1)
        self.assertEqual(len(qobj.get_fw_info()), 1)
        self.assertEqual(qobj.get_fw_info()[0]['state'], 'READY')
        self.assertEqual(len(qobj.get_task_info()), 1)
        self.assertEqual(qobj.get_task_info()[0]['tasks'][0]['name'], 'math.pow')

        fw_query = {'name': 'Square root task'}
        qobj = WFQuery(self.lpad, self.wf_query, fw_query, metadata_only=True)
        self.assertEqual(len(qobj.get_fw_ids()), 1)
        self.assertEqual(len(qobj.get_fw_info()), 1)
        self.assertEqual(qobj.get_fw_info()[0]['state'], 'WAITING')
        self.assertEqual(len(qobj.get_task_info()), 1)
        self.assertEqual(qobj.get_task_info()[0]['tasks'][0]['name'], 'math.sqrt')

    def test_names(self):
        """ test name queries firework-wise """
        fw_query = {'name': 'Power task'}
        qobj = WFQuery(self.lpad, self.wf_query, fw_query)
        self.assertEqual(len(qobj.get_i_names()), 2)
        self.assertIn(('source', int), qobj.get_i_names())
        self.assertIn(('power', int), qobj.get_i_names())
        self.assertEqual(len(qobj.get_o_names()), 1)
        self.assertIn(('number', type(None)), qobj.get_o_names())

    def test_data(self):
        """ test data queries firework-wise """
        fw_query = {'name': 'Power task'}
        qobj = WFQuery(self.lpad, self.wf_query, fw_query)
        number_i = qobj.get_data('number', 'input')
        self.assertEqual(qobj.get_i_data('number'), number_i)
        self.assertEqual(qobj.get_i_data('source')[0]['source'], 2)
        self.assertEqual(qobj.get_i_data('power')[0]['power'], 2)
        self.assertEqual(qobj.get_data('number', 'output'), [])
        self.assertEqual(qobj.get_o_data('number'), [])

    def test_nodes_providing(self):
        """ test the get_nodes_providing() function """
        qobj = WFQuery(self.lpad, self.wf_query, metadata_only=True)
        fw_ids = qobj.get_nodes_providing('number')
        self.assertEqual(len(fw_ids), 1)
        self.assertEqual(qobj.get_fw_info(fw_ids)[0]['name'], 'Power task')
        fw_ids = qobj.get_nodes_providing('^numb', match='regex')
        self.assertEqual(len(fw_ids), 1)
        self.assertEqual(qobj.get_fw_info(fw_ids)[0]['name'], 'Power task')

    def test_metadata_only(self):
        """ test the wfquery class with metadata_only turned on """
        qobj = WFQuery(self.lpad, self.wf_query, metadata_only=True)
        self.assertEqual(qobj.get_i_data('source'), [])
        self.assertEqual(qobj.get_data('power', 'input'), [])
        self.assertIn(('source', type(None)), qobj.get_i_names())
        self.assertIn(('power', type(None)), qobj.get_i_names())


if __name__ == '__main__':
    unittest.main()
