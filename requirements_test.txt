-r requirements.txt
requests
pytz
python-dateutil
flake8
pylint
pytest
pylint-exit
pytest-cov
anybadge
pyflakes
bandit
testfixtures
