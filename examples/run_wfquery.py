"""perform simple workflow queries"""
from fireworks import Firework, Workflow, PyTask
from fireworks.core.launchpad import LaunchPad
from virtmat.middleware.query.wfquery import WFQuery

launchpad = LaunchPad.from_file('launchpad.yaml')

wf_name = 'WFQuery test'
fw_name = 'Square root task'
tasks = [PyTask(func='math.sqrt', inputs=['number'])]
specb = {'number': 16, '_category': 'batch'}
fwb = [Firework(tasks=tasks, spec=specb, name=fw_name+str(i)) for i in range(4)]
speci = {'number': 4, '_category': 'interactive'}
fwi = [Firework(tasks=tasks, spec=speci, name=fw_name+str(i)) for i in range(6)]
fws = fwb + fwi

links = {
    fws[0]: [fws[1], fws[2], fws[3]],
    fws[1]: [fws[3], fws[4], fws[5]],
    fws[2]: [fws[5], fws[6], fws[7]],
    fws[3]: [fws[7], fws[8], fws[9]],
    fws[4]: [],
    fws[5]: [],
    fws[6]: [],
    fws[7]: [],
    fws[8]: [],
    fws[9]: []
}
workflow = Workflow(fireworks=fws, links_dict=links, name=wf_name, metadata={})
launchpad.add_wf(workflow)

wf_query = {'name': {'$regex': '^WFQuery'}}
qobj = WFQuery(launchpad, wf_query)
print(qobj.get_wf_info())
print(qobj.get_wf_ids())
print(qobj.get_fw_info())
print(qobj.get_fw_ids())
print(qobj.get_task_info())
print(qobj.get_i_names())
print(qobj.get_o_names())
print(qobj.get_data('number', 'input'))
print(qobj.get_i_data('number'))
print(qobj.get_data('number', 'output'))
print(qobj.get_o_data('number'))
