"""run a workflow engine with a remote worker"""
from fireworks import LaunchPad
from fireworks.fw_config import LAUNCHPAD_LOC
from fireworks.user_objects.queue_adapters.common_adapter import CommonAdapter
from virtmat.middleware.engine.wfengine_remote import WFEngineRemote

launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
qadapter_dct = {
    '_fw_name': 'CommonAdapter',
    '_fw_q_type': 'SLURM',
    'nodes': 1,
    'ntasks': 1,
    'pre_rocket': None,
    'queue': 'dev_single',
    'rocket_launch': 'rlaunch singleshot',
    'walltime': '00:01:00'
}
qadapter = CommonAdapter.from_dict(qadapter_dct)

wfe = WFEngineRemote(launchpad, qadapter, wf_query=None,
                     launchdir='/path/to/remote/launchdir',
                     host='hostname.domainname', user='username',
                     conf='module load python/3')
wfe.add_workflow(fw_id=4)
wfe.status_detail(4)
update = {'_category': 'remote', '_fworker': wfe.name}
wfe.update_node(fw_id=4, update_dict=update)
wfe.status_detail(4)
wfe.slaunch(fw_id=4)
