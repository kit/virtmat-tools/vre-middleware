This is a [Virtual Research Environment](https://en.wikipedia.org/wiki/Virtual_research_environment) (VRE) middleware that has the goal to provide a Jupyter-oriented application programming interface (API) and a graphical user interface (GUI) to existing computing resources and infrastructures for virtual materials design. This VRE middleware acts as an interface to existing workflow systems and batch systems on different HPC clusters that can be switched as transparent backends. The VRE middleware enables implementing available [VirtMat](https://www.materials.kit.edu/298.php) [use cases](https://gitlab.kit.edu/kit/virtmat-tools/virtmat-models-and-data-analyses) as Jupyter notebooks that can be used, for instance, via the [JupyterHub services](https://www.nhr.kit.edu/userdocs/jupyter/) on computing resources provided by [NHR@KIT](https://www.nhr.kit.edu/english) and [bwUniCluster](https://wiki.bwhpc.de/e/Category:BwUniCluster_2.0).


# Quick start

  * [Prerequisites](docs/prerequisites.md)
  * [Installation and setup](docs/installation.md)
    - [Local installation](docs/installation.md#local-installation)
    - [Development installation](docs/installation.md#development-installation)
  * [Running the unit tests](docs/testing.md#running-the-unit-tests)


# Documentation

The full documentation is available [here](https://vre-middleware.readthedocs.io/).


# Support

If you need support or have any questions about VRE Middleware please write a message to virtmat-tools@lists.kit.edu.


# Continuous Integration (CI) Environment

A more in-depth explanation of the Continuous Integration (CI) environment can be found in the [Docker-Container repository](https://gitlab.com/ikondov/vre-ci-docker/-/blob/master/docs/ci_documentation.md)
