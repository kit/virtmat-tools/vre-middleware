# The `resconfig` command-line tool

The `resconfig` tool allows creating a resource configuration from scratch.
To load and modify an existing configuration one should consider using the Python API.

## Syntax

The command `resconfig --help` yields this help:

```
usage: resconfig [-h] [-i] [-o] [-f PATH]

create a resource configuration file from scratch

optional arguments:
  -h, --help            show this help message and exit
  -i, --interactive     enable interactive mode, no overwrite
  -o, --overwrite       overwrite existing configuration
  -f PATH, --path PATH  non-default resconfig location, overrides RESCONFIG_LOC
```

## Behavior

If a path is specified using the option `-f, --path` that path is used independent of the path specified in the RESCONFIG_LOC environment variable or the default path.

If the resconfig path exists, no resconfig is written. The flag `-o, --overwrite` enables overwriting the file with a newly generated resconfig.

It the `-i, --interactive` flag is specified the user is prompted to confirm creating a new resconfig file. If the resconfig path exists, no action is performed, regardless of `-o, --overwrite` flag.
