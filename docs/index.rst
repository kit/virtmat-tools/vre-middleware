VRE Middleware
==============

This `middleware <https://en.wikipedia.org/wiki/Middleware>`_ package aims to create a platform for 
a `Virtual Research Environment (VRE) <https://en.wikipedia.org/wiki/Virtual_research_environment>`_
for scientific computing. This platform integrates high performance computing (HPC) resources
(currently via `Slurm <https://slurm.schedmd.com/>`_), workflow management systems (currently
`FireWorks <https://materialsproject.github.io/fireworks/>`_) and MongoDB database, that are switched
as transparent backends. The middleware provides three user interfaces:

* a `Jupyter <https://jupyter.org/>`_-oriented application programming interface (API);
* a graphical user interface (GUI) via `Jupyter widgets <https://ipywidgets.readthedocs.io/en/latest/>`_;
* a command-line interface (CLI).

The middleware includes three major components:

* WFEngine: A workflow engine for managing FireWorks workflows and their nodes conveniently in a Jupyter notebook;
* WFQuery: A query tool for FireWorks workflows: retrieve data from workflows and their nodes;
* ResConfig: A resource configuration manager: manage Slurm queues, environment modules and much more.


.. toctree::
   :maxdepth: 2
   :caption: Quick start

   prerequisites
   installation
   testing
   
.. toctree::
   :maxdepth: 2
   :caption: Using the API

   wfengine
   wfquery
   resconfig
   launchpad
   qadapter
   wfengine_remote

.. toctree::
   :maxdepth: 2
   :caption: Using the GUI

   ipywidgetsgui

.. toctree::
   :maxdepth: 2
   :caption: Using the CLI

   wfengine_cli
   resconfig_cli

.. toctree::
   :maxdepth: 2
   :caption: Help

   troubleshooting
   faqs

.. toctree::
   :maxdepth: 2
   :caption: Comprehensive documentation

   source/virtmat

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
