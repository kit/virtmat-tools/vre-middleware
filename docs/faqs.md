# Frequently asked questions (FAQs)

**Q**: How can I avoid having launch directories (`launcher_YYYY-DD-MM-HH-mm-NN`) everywhere in my system?

**A**: This can be achieved by setting up a *default launchdir* in the resource configuration, `res_config`. All new launch folders, and data, from the worker's launches will be stored in the configured *default launchdir*.
The set-up of the `default_launchdir` can be done using the vre-middleware's Python API:
```python
from virtmat.middleware.resconfig import get_resconfig_loc, get_default_resconfig
cfg = get_default_resconfig()
wcfg = cfg.workers[0] #NOTE
wcfg.default_launchdir = '/path/to/my/desired/default_launchdir'
cfg.default_worker = wcfg
cfg.to_file(get_resconfig_loc())
```
**NOTE:** In this example `worker[0]` is assumed as the relevant worker for the launchdir configuration. If available, other workers can be selected by using the corresponding index, eg `worker[n]`.