# Running the unit tests

First, make sure that all requirements for testing are fullfilled and then run pytest:

```bash
$ python -m pip install .[test]
$ cd tests
$ pytest .
```

To run only the tests that do not need slurm use this command:

```bash
$ cd tests
$ pytest -m "not slurm" .
```
