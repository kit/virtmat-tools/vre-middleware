# Installation and setup

It is recommended to setup a [Python virtual environment](https://docs.python.org/3/library/venv.html) first. All installation methods outlined below require the `pip` package. First make sure you have the most recent pip:

```bash
$ python -m pip install --upgrade pip 
```


## Installation from the pypi.org repository

This is the recommended way to install the latest release:

```bash
$ python -m pip install vre-middleware 
```


## Local installation from source

An installation from source requires to either clone the repository or to download a source archive file from [here](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/releases). Here, we show how to use the first method and then to install VRE Middleware using `pip`:

```bash
$ git clone https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware.git
$ cd vre-middleware
$ python -m pip install .
```

This command will install VRE mdiddleware and all dependency packages into your [Python virtual environment](https://docs.python.org/3/library/venv.html). 


## Development installation

You can install VRE Middleware in such a way that changes in the local repository source tree become immediately available without re-installation of the package:

```bash
$ cd vre-middleware
$ python -m pip install -e .
```

After installing the packages, the modules from VRE Middleware can be loaded, for example

```python
from virtmat.middleware.query.wfquery import WFQuery
```

**NOTE: Broken install due to an open issue in FireWorks**

Fresh installations of VRE Middleware will currently *sometimes* not work. When running the API or the GUI this error may occur:

```
ModuleNotFoundError: No module named 'pytest'
```

The error is documented in this [issue](https://github.com/materialsproject/fireworks/issues/535).

As a work-around, the `pytest` package can be installed via this command:

```
python -m pip install pytest
```

## Setting up FireWorks with Mongomock

The FireWorks workflow management system needs a data store to persist data and metadata. In some limitted number of use cases, such as with a single user, single process, single computer, `mongomock` can be used as a simple local store.

### Steps

1. Install the `mongomock-persistence` package:
    ```bash
    python -m pip install mongomock-persistence
    ```

1. Create and initialize the file `mongomock.json` for the mock database and add its absolute path to the FireWorks main configuration file by the following commands:
    ```
    mkdir -p $HOME/.fireworks
    echo '{}' > $HOME/.fireworks/mongomock.json
    echo MONGOMOCK_SERVERSTORE_FILE: $HOME/.fireworks/mongomock.json >> $HOME/.fireworks/FW_config.yaml
    lpad reset
    ```

## Setting up FireWorks with a MongoDB instance

Production use of VRE-Middleware, i.e. running multiple processes and/or using several computers or computing nodes, requires that FireWorks uses a MongoDB database. This short guide shows how to configure FireWorks with an existing database running on a MongoDB instance in the cloud.

### Steps

1. Create directory `$HOME/.fireworks` with:
    ```bash
    mkdir -p $HOME/.fireworks
    ```
1. Create the [LaunchPad](launchpad.md) file `launchpad.yaml` with the following contents:

    ```yaml
    host: <hostname of MongoDB instance>
    port: 27017
    name: <database name>
    username: <your username>
    password: <your password>
    mongoclient_kwargs:
        tls: true
        tlsCAFile: <absolute path to CA certificate>
        tlsCertificateKeyFile: <absolute path to client certificate>
    ```

    The two certificate files (in *.pem format), the hostname, the username, and the password, you can get from the provider of the MongoDB service. These connection and access data have to be filled at the relevant places, and the absolute paths (i.e. beginning with `/`) of the two certificate files have to be set correctly.
1. Create the main FireWorks configuration file and set the absolute path of the LaunchPad file in it:
 
    ```bash
    echo LAUNCHPAD_LOC: $HOME/.fireworks/launchpad.yaml >> $HOME/.fireworks/FW_config.yaml
    ```
    Remove or comment out the line with the key `MONGOMOCK_SERVERSTORE_FILE` in `FW_config.yaml` if appropriate.
1. Test the access, for example by using the command
    ```bash
    lpad get_wflows -d count
    ```
    that will print the number of workflows in the database.
1. If the database has just been created: reset the LaunchPad so that all necessary collections will be initialized:
    ```
    lpad reset
    ``` 