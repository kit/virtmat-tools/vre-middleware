# How to create a qadapter object

If your workflow includes nodes that require large computing resources that are supposed to be run using a *batch queuing system* a qadapter object must be specified. The qadapter file contains the batch queuing system type (such as SLURM, PBS, SGE, and IBM LoadLeveler), the number of nodes and cores, tasks per node, total memory, job walltime, type of resources (GPU, CPU) and account name.

There are several approaches to create a qadapter object.

1. Create a default qadapter from a [resource configuration](resconfig.md). This is the recommended approach if the default resources in resconfig satisfy the requirements.

    ```python
    from virtmat.middleware.resconfig import get_default_qadapter
    qadapter = get_default_qadapter()
    ```

    **NOTE:** If the qadapter will be used in `WFEngine` running with a custom launchpad, then a path to the custom launchpad file has to be passed, i.e.

    ```python
    qadapter = get_default_qadapter(lp_file=/absolute/path/to/launchpad.yaml)
    ```

1. Create a custom qadapter from a [resource configuration](resconfig.md). This is the recommended approach if the default resources configured in resconfig do not satisfy the requirements.

    ```python
    from virtmat.middleware.resconfig import get_custom_qadapter
    w_name, qadapter = get_custom_qadapter(ntasks_per_node=20,
                                           cpus_per_task=2, walltime=10)
    ```

    The call of `get_custom_qadapter` returns the name `w_name` of the first matching worker and the qadapter for that specific worker. One can explicitly pass a resconfig object (`cfg=resconfig` or as first positional argument) and a worker from the list of configured workers, by passing `w_name=wcfg.name` as a keyword argument, where `wcfg` is the manually selected worker. In addition, if `w_name` is provided, one can explicitly select a queue from the list of configured queues, e.g. `q_name=wcfg.queues[2].name` selects the third queue configured in the worker. There is no need to select default worker and queue explicitly because they will be considered first and selected if they satisfy the resource requirements. If no matching resources can be found in the resource configuration then `(None, None)` is returned.

    **NOTE:** If the qadapter will be used in `WFEngine` running with a custom launchpad, then a path to the custom launchpad file has to be passed, i.e.:

    ```python
    qadapter = get_custom_qadapter(lp_file=/absolute/path/to/launchpad.yaml, **resources)
    ```

    Keys supported in the `resources` dictionary:
    * `nodes` (`int`): Number of computing nodes
    * `ntasks_per_node` (`int`): Number of tasks (processes) per computing node
    * `cpus_per_task` (`int`): Number of processor cores per task - this is usually the number of threads
    * `walltime` (`int`): The wall time requirement for the job, in minutes
    * `mem_per_cpu` (`int`): The memory (in MB) required by one thread
    * `modules` (`dict`): A dictionary with environment modules to be loaded. The keys are the module names, the values are version requirement specifications, e.g. `>=0.4.5`. The most recent version will be chosen if If the specification is `None`.
    * `envs` (`dict`): a dictionary with environment variables to be set, the keys are the names of variables, the values are their values (must be strings). The variable will be unset if the value is `None`.

1. Create a qadapter from the default qadapter file (if configured properly in `$HOME/.fireworks`)

    ```python
    from fireworks.fw_config import QUEUEADAPTER_LOC
    from fireworks.user_objects.queue_adapters.common_adapter import CommonAdapter
    qadapter = CommonAdapter.from_file(QUEUEADAPTER_LOC)
    ```

    **NOTE:** If the qadapter will be used in `WFEngine` running with a custom launchpad, then it must be specified by

    ```yaml
    rocket_launch: rlaunch -l /absolute/path/to/launchpad.yaml singleshot
    ```

1. Create a custom qadapter using a dictionary

    ```python
    from fireworks.user_objects.queue_adapters.common_adapter import CommonAdapter
    qadapter_dct = {
        '_fw_name': 'CommonAdapter',
        '_fw_q_type': 'SLURM',
        'nodes': 1,
        'ntasks': 1,
        'pre_rocket': None,
        'queue': 'dev_single',
        'rocket_launch': 'rlaunch singleshot',
        'walltime': '00:01:00'
    }
    qadapter = CommonAdapter.from_dict(qadapter_dct)
    ```

    **NOTE:** If the qadapter will be used in `WFEngine` running with a custom launchpad, then it must be specified by

    ```python
    qadapter_dct['rocket_launch'] = 'rlaunch -l /absolute/path/to/launchpad.yaml singleshot'
    ```

1. Create a custom qadapter from a JSON or YAML file:

    ```python
    from fireworks.user_objects.queue_adapters.common_adapter import CommonAdapter
    qadapter = CommonAdapter.from_file('qadapter.yaml')
    ```

More technically interested users can look at the [full documentation](https://materialsproject.github.io/fireworks/fireworks.user_objects.queue_adapters.html?highlight=commonadapter) and read [this tutorial](https://materialsproject.github.io/fireworks/qadapter_programming.html?highlight=commonadapter).
