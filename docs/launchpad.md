# How to create a LaunchPad object

The LaunchPad object establishes a connection to a FireWorks database (the LaunchPad). To construct it, we need at least the hostname of the MongoDB instance running the database and the name of the database itself. All the other parameters are optional. If the MongoDB instance has enabled authentication, also username and password must be specified. [This guide](installation.md#setting-up-fireworks-with-a-mongodb-instance) shows how to configure FireWorks to use an existing MongoDB instance.

There are several ways to create the LaunchPad object:

1. Use the default LaunchPad file, if configured properly in `$HOME/.fireworks/FW_config.yaml` (see [this guide](installation.md#setting-up-fireworks-with-a-mongodb-instance)):

    ```python
    from fireworks import LaunchPad
    from fireworks.fw_config import LAUNCHPAD_LOC
    launchpad = LaunchPad.from_file(LAUNCHPAD_LOC)
    ```

1. Create a custom launchpad for a MongoDB instance running locally without SSL and without authentication:

    ```python
    from fireworks import LaunchPad
    launchpad = LaunchPad()
    ```

1. Create a launchpad from an existing JSON or YAML file:

    ```python
    from fireworks import LaunchPad
    launchpad = LaunchPad.from_file('launchpad.yaml')
    ```

    or from a dictionary (in this example for a MongoDB instance running locally without SSL and without authentication):

    ```python
    from fireworks import LaunchPad
    launchpad = LaunchPad.from_dict({'host': 'localhost', 'port': 27017, 'name': 'fireworks'})
    ```

A full documentation of the LaunchPad class is provided [here](https://materialsproject.github.io/fireworks/fireworks.core.html?highlight=launchpad#fireworks.core.launchpad.LaunchPad).
