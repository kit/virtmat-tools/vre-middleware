# Prerequisites

The minimum Python version required to use VRE Middleware is currently 3.9. The best is to have access to JupyterLab, either locally or on HPC resources via [JupyterHub services](https://wiki.bwhpc.de/e/Jupyter_at_SCC). Your [Python virtual environment](https://docs.python.org/3/library/venv.html) must contain a Jupyter kernel and it must be registered in Jupyter:

```bash
$ python -m ipykernel install --user --name=<venv-name>
```

where `<venv-name>` must be replaced with the name of your [Python virtual environment](https://docs.python.org/3/library/venv.html). To check your registered Jupyter kernels use this command:

```bash
$ jupyter kernelspec list
Available kernels:
  <venv-name>    </path/to/home/.local/share/jupyter/kernels/<venv-name>
```
