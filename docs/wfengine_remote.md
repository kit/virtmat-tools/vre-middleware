# Running a launcher on a remote computer

The launcher threads of `WFEngine` objects always run on the local host. The subclass `WFEngineRemote` allows running a launcher on a remote host.

## How to create a `WFEngineRemote` object

The snippet below shows how to creates a `WFEngineRemote` object that will launch workflow nodes on the remote host `samplehost.sampledomain` in the account of user `xy1234` and configuration command `module load python`:

```python
from virtmat.middleware.engine.wfengine_remote import WFEngineRemote

wfe = WFEngineRemote(launchpad=launchpad, qadapter=qadapter, wf_query=wf_query,
                     host='samplehost.sampledomain', user='xy1234', conf='module load python')
```

The following parameters are specific for the WFEngineRemote class and configure the access to the remote host:

- `host` (str) : the [fully qualified domain name](https://en.wikipedia.org/wiki/Fully_qualified_domain_name) of the remote host on which to execute the launch command.
- `user` (str): username for SSH to authenticate the user with paswordless SSH key.
- `conf` (str): configuration command to set up the remote environment (multiple commands should be separated with `;`)

The other parameters are inherited from the parent class [`WFEngine`](wfengine.md). More information about them can be found [here](wfengine.md#create-a-wfengine-object).

**Passwordless connection via SSH to the remote machine must be enabled. Otherwise the following error message will occur:**
    
    PasswordRequiredException: private key file is encrypted

The next section provides more information about passwordless SSH.

## Using SSH to activate a remote launcher

The following lines will help you to establish a secure paswordless SSH connection. Some basic knowledge of SSH is required to complete these steps.

1. In order to login on the remote machine in a secure fashion, you need to use an SSH protocol. This requires an SSH key generation to login to server without password. To achieve this, run the command below; you will be prompted to enter a passphrase. Please choose a secure one and avoid an empty passphrase.
    
        ssh-keygen

2. Afterwards, you need to add your public key on the remote machine by issuing the command below. You will be prompted for the key passphrase that you set in the previous step.

        ssh-copy-id USERNAME@server

3. Afterwards, you need to add your SSH key to ssh-agent in order to enable paswordless SSH login. This can be done by issuing the command below:

        eval $(ssh-agent) ssh-add

Again you should enter your passphrase to add your key to agent.

Finally, you should be able to login on the remote machine without any password or passphrase prompt by issuing the command bellow. 
    
    ssh USERNAME@server
