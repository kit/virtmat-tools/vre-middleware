# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)

## [Unreleased]

## [1.2.4] - 2025-03-05

### Added
- **docs**: faq entry to configure a default launchdir for a worker ([#126](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/126))
- **packaging**: add project repository, docs, and further URLs ([#120](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/120))
- **docs**: add the CLI to top paragraph in docs ([#118](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/118))
- **wfengine**: apply cancellation policy to reserved nodes ([#74](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/74))
- **wfengine**: a method to restart a node from DEFUSED state ([#68](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/68))

### Fixed
- **wfengine**: solve performance issues in several database queries ([#125](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/125))
- **wfengine**: make on-demand evaluation work correctly ([#123](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/123))
- **docs**: docs formatting and grammar ([#122](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/122))
- **wfengine**: handle correctly jobs in RESERVED state ([#71](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/71))
- **wfengine**: getting SLURM job state with method=parse in some job states ([#70](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/70))

### Changed
- **wfengine**: `get_lost_jobs()` does not fizzle nodes by default ([#124](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/124))
- **tests**: increase coverage precision ([#119](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/119))

## [1.2.3] - 2025-01-31

### Added
- **resconfig**: add variable definitions and simple commands to resconfig ([#115](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/115))
- **docs**: complete installation guide ([#113](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/113))
- **docs**: add a guide for configuration of FireWorks with MongoDB ([#112](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/112))

### Changed
- **packaging**: adapt for new fireworks release ([#116](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/116))
- **docs**: extend the docs about worker config ([#111](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/111))


## [1.2.2] - 2024-11-14
### Added
- **logo**: add project logo to repository and documentation site ([#107](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/107))
- **wfengine**: add a CLI tool to create WFEngine objects and run background launcher threads ([#106](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/106))

### Fixed
- **resconfig**: allow None as default queue for workers without queues ([#108](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/108))
- **docs**: update incorrect paths for module imports ([#104](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/104))


## [1.2.1] - 2024-09-24
### Added
- **resconfig**: add a default launchdir ([#102](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/102))
- **docs**: add an introduction paragraph to index ([#99](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/99))
- **tests**: include tests for SLURM coverage ([#97](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/97))
- **docs**: add a CHANGELOG ([#95](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/95))
- **tests**: add coverage to CI-pipeline ([#94](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/94))
- **docs**: include a trouble-shooting section ([#73](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/73))

### Fixed
- **wfengine**: enable use of launchdir for interactive nodes ([#110](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/110))
- **docs**: fix non-displayed figures in the docs ([#96](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/96))

### Changed
- **docs**: improve docs sections for create engine and resconfig ([#101](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/101))
- **packaging**: create a workaround for fireworks problems in new installations ([#100](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/100))


## [1.2.0] - 2024-05-31
### Added
- **release**: make new release ([#93](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/93))
- **gui**: provide GUI for resconfig and qadapter setup ([#81](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/81))


### Fixed
- **resconfig**: fix automatic creation of resconfig in HoreKa ([#91](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/91))
- **resconfig**: filter out invalid resources provided by SLURM ([#88](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/88))
- **resconfig**: fix setup crash caused by missing `$HOME/.fireworks` ([#85](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/85))

### Changed
- **resconfig**: add currently loaded modules in pre_rocket ([#92](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/92))
- **wfengine**: allow launcher thread to run all nodes in READY state before sleeping ([#90](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/90))
- **resconfig**: split validation from construction of custom qadapter ([#89](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/89))
- **resconfig**: support pre_rocket in default and custom qadapters from resconfig ([#87](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/87))
- **resconfig**: create default resconfig for systems without SLURM ([#84](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/84))
- **wfengine**: employ default qadapter from resconfig in WFEngine class ([#82](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/82))
- **resconfig**: specify location of non-default launchpad in qadapter object ([#77](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/77))
- **wfengine**: allow optional creation of `launchdir_*` folders for interactive nodes ([#72](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/72))


## [1.1.1] - 2024-02-20
### Fixed
- **packaging**: restrict supported python versions to >= 3.9 ([#83](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/83))

### Changed
- **release**: correct version of the release
- **docs**: update contributor list


## [1.1.0] - 2024-02-16
### Added
- **gui**: add interactive construction of qadapter ([#69](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/69))
- **release**: make first release ([#76](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/76))
- **tests**: add bandit to testing suite ([#51](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/51))
- **wfengine**: enable loading of engine from file selector ([#43](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/43))
- **wfengine**: allow resetting fireworks in RESERVED state ([#41](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/41))
- **wfengine**: add wf_query, fw_ids, and fw_ids as properties ([#37](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/37))
- **docs**: write documentation for remote worker engine class ([#35](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/35))
- **wfquery**: add key to control data download ([#32](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/32))
- **docs**: write documentation for the WFEngine class ([#18](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/18))

### Fixed
- **docs**: correct documentation for add_workflow function ([#78](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/78))
- **tests**: fix stuck test_cancel_running_job ([#55](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/55))
- **wfengine**: enable resuming of remote engine from saved file ([#44](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/44))
- **gui**: display `firework id` field next to `remove workflow` ([#40](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/40))
- **wfengine**: fix compliance of remote worker launchpad with new FireWorks ([#39](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/39))
- **tests**: fix test crash with non-empty database ([#23](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/23))
- **gui**: fix non-working `Remove workflow` button ([#22](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/22))
- **wfengine**: launch interactive nodes in unique launchdir ([#19](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/19))
- **wfengine**: print correct information for `status_summary()` ([#15](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/15))
- **wfengine**: fix addition of new workflow ([#14](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/14))
- **utilities**: fix settings for global logging ([#79](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/79))
- **gui**: improve GUI usability ([#66](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/66))
- **wfengine**: allow definition of constant functions ([#64](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/64))
- **gui**: fix non-working gui ([#63](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/63))
- **wfengine**: ensure qlaunch starts SLURM tasks ([#50](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/50))
- **wfengine**: fix Module not found error in remote wfengine ([#47](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/47))
- **wfengine**: fix deserialization ([#42](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/42))
- **wfengine**: fix delete workflow feature ([#20](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/20))
- **wfengine**: fix issues with `status_summary` and `statur_detail` function ([#11](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/11))

### Changed
- **docs**: update git instance links to gitlab.kit.edu ([#80](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/80))
- **packaging**: separate base and test requirements, increase python version ([#62](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/62))
- **wfengine**: allow running specific nodes ([#60](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/60))
- **packaging**: create common namespace for vre-language and vre-middleware ([#61](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/61))
- **packaging**: change package name from vremiddleware to vre-middleware ([#59](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/59))
- **ci**: fix enroot import path ([#57](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/57))
- **docs**: improve top-level README ([#56](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/56))
- **ci**: use a separate FW_config for CI ([#53](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/53))
- **packaging**: change package name to enable common namespaces ([#52](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/52))
- **gui**: reshape status button ([#48](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/48))
- **wfengine**: detect RESERVED nodes whose SLURM jobs crashed ([#46](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/46))
- **wfengine**: produce diagnostic messages using loggers  from main and launcher threads ([#45](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/45))
- **wfengine**: refactor the WFEngine class ([#31](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/31))
- **wfengine**: refactor the status_summary function ([#30](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/30))
- **wfengine**: indicate protected attributes ([#29](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/29))
- **wfengine**: review restart and pause keywords in the `cancel_job()` function ([#28](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/28))
- **wfengine**: make qadapter and wf_query optional arguments in WFEngine class ([#27](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/27))
- **wfengine**: remove fworker object from WFEngine class ([#17](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/17))
- **wfengine**: allow creation of an engine with no workflows ([#13](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/13))
- **wfengine**: enable `start()` and `stop()` functions to test for running launcher thread ([#10](https://gitlab.kit.edu/kit/virtmat-tools/vre-middleware/-/issues/10))


## [1.0.1] - 2023-01-19
### Fixed
- **docs**: correct list of contributors


## [1.0.0] - 2023-01-19
### Added
- **wfquery**: allow expand/reduce table output from WFQuery  (#38)
- **gui**: add `name` keyword of WFEnine to widgets interface (#36)
- **docs**: document ipywidgets GUI (#34)
- **ci**: implement CI workflow for unit tests (#24)
- **gui**: complete GUI for existing functions of WFEngine class (#21)
- **wfengine**: enable cancellation of a reserved or running job  (#16)
- **docs**: document how to perform and use queries (#12)
- **tests**: add unit testing (#9)
- **wfquery**: add simple query functions (#8)
- **wfengine**: enable firework editing (#7)
- **wfengine**: redesign and failure handling (#6)
- **wfengine**: enable saving and loading of engine object (#5)
- **wfengine**: enable adding and extending workflows (#4)
- **wfengine**: allow scope management of engine (#3)
- **wfengine**: add monitoring and diagnostic functions (#2)
- **wfengine**: manage execution of batch and interactive fireworks (#1)

### Fixed
- **ci**: fix folders cluttering $HOME in CI node (#58)

### Changed
- **wfengine**: refactor add node category, include fworker and qadapter in add_node function (#25)
